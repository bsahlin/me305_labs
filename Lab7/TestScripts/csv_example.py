#from matplotlib import pyplot as plt

# Blank lists for data. On the Nucleo you should probably use array.array() to
# better storage density. Make sure you pick the right data type when you init
# the array if you do go with array.array instead of a list.
from array import array
time = array('f')
velocity = array('f')
position = array('f')

# Open the file. From here it is essentially identical to serial comms. There
# are better ways to do this, like using the 'with' statement, but using an
# object like this is closest to the serial code you've already used.
ref = open('reference.csv');

# Read data indefinitely. Loop will break out when the file is done.
#
# You will need to handle this slightly differently on the Nucleo or
# pre-process the provided data, because there are 15,001 rows of data in the
# CSV which will be too much to store in RAM on the Nucleo.
#
# Consider resampling the data, interpolating the data, or manipulating the 
# data in some fashion to limit the number of rows based on the 'interval' for
# your control task. If your controller runs every 20ms you do not need the
# data sampled at 1ms provided by the file.
#
while True:
    # Read a line of data. It should be in the format 't,v,x\n' but when the
    # file runs out of lines the lines will return as empty strings, i.e. ''
    line = ref.readline()
    
    # If the line is empty, there are no more rows so exit the loop
    if line == '':
        break
    
    # If the line is not empty, strip special characters, split on commas, and
    # then append each value to its list.
    else:
        (t,v,x) = line.strip().split(',');
        time.append(float(t))
        velocity.append(float(v))
        position.append(float(x))

# Can't forget to close the serial port
ref.close()

'''
# To make the example complete, plot the data as a subplot.
plt.figure(1)

# Velocity first
plt.subplot(2,1,1)
plt.plot(time,velocity)
plt.ylabel('Velocity [RPM]')

# Then position
plt.subplot(2,1,2)
plt.plot(time,position)
plt.xlabel('Time [s]')
plt.ylabel('Position [deg]')
'''