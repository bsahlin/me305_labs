# -*- coding: utf-8 -*-
"""
Created on Fri Nov 27 18:02:58 2020

@author: ben
"""

import ujson

d_send = dict()
d_send[1] = [2,4]
d_send[2] = [3,5]
d_json_send = ujson.dumps(d_send)
d_read = ujson.loads(d_json_send)
#gives dict object with string key and integer tuple
#change to int keys but keep integer tuples same
d_read = {int(k):v for k,v in d_read.items()}
