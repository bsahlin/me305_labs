# -*- coding: utf-8 -*-
"""
Created on Sat Nov 28 19:38:33 2020

@author: ben
"""

from pyb import USB_VCP as uv
import ujson
import utime
from array import array

usbv = uv(0)

tarray = array('f')
varray = array('f')

while True:
    #simulate backend runing every 10ms
    utime.sleep_ms(20)
    if usbv.any() >= 1:
        (t,v)= usbv.readline().decode('ascii').strip().split(',')
        tarray.append(float(t))
        varray.append(float(v))
        #usbv.write('\n'.encode('ascii'))
        '''
        try:
            readjson = ujson.loads(readstring)
            tarray.append(readjson.keys)
            #usbv.write('JSON parse successful\r\n')
        except ValueError:
            usbv.write('JSON parse error\r\n')
            usbv.write('parrot: \r\n')
            usbv.write('{:}\r\n'.format(readstring).encode('ascii'))            
            break
        '''
        #usbv.write('parrot: \r\n')
        usbv.write('{:},{:}\r\n'.format(t,v).encode('ascii'))