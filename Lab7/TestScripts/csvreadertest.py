# -*- coding: utf-8 -*-
"""
Created on Fri Nov 27 14:39:21 2020

@author: ben
"""

#use as reference for csv importing from file

import csv
'''
with open('reference.csv', newline = '') as csvfile:
    reader = csv.DictReader(csvfile, fieldnames=['tref','Vref','Xref'])
    for row in reader:
        print(row['tref'], row['Vref'])#, row['Xref'])
'''       

dRef = dict()        
with open('reference.csv', newline = '') as csvf:
    reader = csv.reader(csvf)
    for row in reader:
        #print(row)
        #to get first element (time)
        #print(row[0])
        #match row elements to dictionary creation
        #key is time in microseconds, value is tuple of velocity and position
        dRef[int(float(row[0])*1e6)] = [row[1], row[2]]
        #print(dRef)
        