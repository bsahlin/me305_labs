# -*- coding: utf-8 -*-
'''
@file       FSM_CLController.py

@brief      Task for closed loop motor control
@details    Includes finite state machine for running closed loop controller 
            task for Lab 06. Uses MotorDriver, EncoderDriver, and controller \
            objects to achieve closed loop motor control.

@author     Ben Sahlin
@date       November 25, 2020
'''

import utime
import shares7 as shares
from array import array

class taskCLController:
    #Define states
    S0_Init             = 0
    S1_WaitForCollect   = 1
    S2_OutputPosition   = 2
    
    def __init__(self, run_interval, collect_time, controller, encoder, motor):
        '''
        @brief                  Creates a taskCLController object
        @param run_interval     Time between runs in microseconds
        @param collect_time     Data collection time length in microseconds
        @param controller       Controller object used in closed loop configuration
        @param encoder          An EncoderDriver object used to collect encoder data
        @param motor            A MotorDriver object used to control motor
        '''
        #Toggle optional debug print statements
        self.debug = False        
        self.run_interval = run_interval
        self.collect_time = collect_time
        self.controller = controller
        self.encoder = encoder
        self.motor = motor
        #ensure motor control is disabled
        self.motor.disable()
        self.current_time = utime.ticks_us()
        self.next_time = utime.ticks_add(self.current_time, self.run_interval)
        print('taskCLController object created using {:} controller'.format(self.controller))
        self.state = self.S0_Init
        
    def run(self):
        '''
        @brief Runs one task iteration
        '''
        self.current_time = utime.ticks_us()
        if utime.ticks_diff(self.current_time, self.next_time) > 0:
            self.next_time = utime.ticks_add(self.current_time, self.run_interval)
            if(self.state == self.S0_Init):
                #state 0
                self.transitionTo(self.S1_WaitForCollect)
            elif(self.state == self.S1_WaitForCollect):
                #state 1
                if shares.collect == True:
                    #start time is data collection start time
                    self.start_time = self.current_time
                    #data collect time is equal to end of ref array time convert to microseconds
                    self.collect_time = int(shares.condensed_tRefArray[-1]*1e6)
                    #end time is data collect time plus current time
                    self.end_collect_time = utime.ticks_add(self.current_time, self.collect_time)
                    self.encoder.set_position(0)
                    self.run_number = 0
                    #clear data collection arrays
                    shares.encoder_position_array = array('i')
                    shares.time_array = array('i')
                    shares.encoder_velocity_array = array('f')
                    #set proportional controller Kp to user input
                    self.controller.set_Kp(shares.Kp)
                    self.debugprint('Controller Kp set to {:}'.format(self.controller.get_Kp()))
                    #enable motor control
                    self.motor.enable()
                    #set up initial velocity reference values
                    self.ti = 0
                    #self.tref = shares.condensed_tRefArray[self.ti]
                    #convert to deg/s from rpm
                    self.Vref = shares.condensed_vRefArray[self.ti]*60
                    
                    self.transitionTo(self.S2_OutputPosition)
                    
            elif(self.state == self.S2_OutputPosition):
                #state 2
                if utime.ticks_diff(self.current_time, self.end_collect_time) > 0:
                    #data collection done
                    shares.collect = False
                    self.motor.disable()
                    self.transitionTo(self.S1_WaitForCollect)
                else:
                    #collect data point
                    self.encoder.update()
                    self.update_position_array()
                    self.update_time_array()
                    #self.debugprint('Encoder at {:} degrees'.format(str(float(self.encoder.get_position())/self.encoder.ticks_per_deg)))
                    #send encoder delta to controller
                    #self.debugprint('Encoder delta {:} degrees'.format(str(float(self.encoder.get_delta()/self.encoder.ticks_per_deg))))
                    #deg/sec = (ticks/run)/(ticks/deg)/((usec/run)/(1e6 usec/sec))
                    
                    self.Vmeas = float(self.encoder.get_delta()/self.encoder.ticks_per_deg/(self.run_interval/1e6))
                    
                    #update reference velocity
                    if self.ti < len(shares.condensed_tRefArray)-1:
                        #compare next ref change time to recorded time from this run in microseconds
                        if round(shares.condensed_tRefArray[self.ti+1]*1e6) <= shares.time_array[-1]:
                            self.ti +=1
                            #self.tref = shares.condensed_tRefArray[self.ti]
                            #deg/s = rev/min * 360 deg/rev * min/60sec
                            self.Vref = shares.condensed_vRefArray[self.ti]*60
                    
                    self.update_velocity_array()                    
                    #controller takes in values in deg/s
                    self.controller.update(self.Vref, self.Vmeas)
                    print('give controller vref {:} vmeas {:} time {:}, get {:}'.format(self.Vref, self.Vmeas, shares.time_array[-1]/1e6, self.controller.L))                    
                    #send L value to motor driver
                    self.motor.setDuty(-1*self.controller.L)
                
            else:
                print('Invalid state code')
                
    def transitionTo(self, next_state):
        '''
        @brief              Sets taskUI state to specified next state
        @param next_state   Variable representing the next state for the FSM
        '''
        self.state = next_state
        self.debugprint('CLController Switch to state '+ str(next_state))
        
    def update_time_array(self):
        #append time after start in microseconds to end of time array
        shares.time_array.append(utime.ticks_diff(self.current_time, self.start_time))
        
    def update_position_array(self):
        '''
        '''
        shares.encoder_position_array.append(round(self.encoder.get_position()/self.encoder.ticks_per_deg))
    
    def update_velocity_array(self):
        '''
        '''
        shares.encoder_velocity_array.append(self.Vmeas)
        
    def debugprint(self, debugstring):
        '''
        @brief                  Prints string if debug parameter is true
        @details                Used for development purposes
        @param debugstring      String to print
        '''
        if self.debug == True:
            print(debugstring)
# =============================================================================
#             
#     def readRefVals(self, filename):
#         '''
#         @brief              Reads and condenses velocity and time arrays
#         @details            Reads arrays from specified CSV file and saves in shares. Also sets length of time to collect data.
#         @param filename     CSV file to read. Time first column (ms), velocity second column (rpm), position third column (deg)
#         '''
#         self.debugprint('Reading reference values')
#         #only read reference values at closed loop interval
#         #note this takes a while, call before entering run loop in main
#         shares.VRef_array = array('f')
#         self.run_int_ms = int(self.run_interval/1e3)
#         '''
#         #try creating array of length 15000/40
#         for i in range(int(15e3/40)):    
#             shares.VRef_array.append(0.0)
#         '''
#         with open(filename, 'r') as csvf:
#             i = 0
#             for line in csvf:
#                 line_Str = csvf.readline().strip().split(',')
#                 #print(i)
#                 if i % self.run_int_ms == 0:
#                     #print(i)
#                     #shares.VRef_array[int(i/40)] = float(line_Str[1])
#                     shares.VRef_array.append(float(line_Str[1]))
#                 i += 1
#             csvf.close()
#             self.debugprint(shares.VRef_array)
#   
#         shares.tRef_array = array('i')
#         #range resets i automaticallly
#         for i in range(len(shares.VRef_array)):
#             shares.tRef_array.append(i*self.run_int_ms)
#         self.debugprint(shares.tRef_array)
# 
#         [shares.VRef_array, shares.tRef_array] = self.condenseRefVals(shares.VRef_array, shares.tRef_array)
#         
#         self.debugprint('Condensed velocity array\r\n{:}'.format(shares.VRef_array))
#         self.debugprint('Condensed time array\r\n{:}'.format(shares.tRef_array))
#         
#     def condenseRefVals(self, vArray, tArray):
#         '''
#         @brief          Part of readRefValues
#         @details        Gets rid of values that aren't different from the previous value, also 
#                         creates reference time array with corresponding times.
#         @param vArray   Velocity array
#         @param tArray   Time array
#         '''
#         self.debugprint('Condensing reference values')
#         prev_val = None
#         condensed_VRef = array('f')
#         condensed_tRef = array('i')
#         for i in range(len(vArray)):
#             current_val = vArray[i]
#             current_time = tArray[i]
#             #if on first iteration or current is not repeat
#             if prev_val == None or current_val != prev_val:
#                 condensed_VRef.append(current_val)
#                 condensed_tRef.append(current_time)
#             prev_val = current_val
#         #set original arrays to condensed arrays
#         print('here{:}'.format(condensed_VRef))
#         self.debugprint('Reference values condensed')
#         return [condensed_VRef, condensed_tRef]
# 
#         
#         
#         
# =============================================================================
