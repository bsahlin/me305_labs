var ui__front__PropControl__lab7_8py =
[
    [ "calc_perf_metric", "ui__front__PropControl__lab7_8py.html#a3091a17ddb56d903f38c32056d463212", null ],
    [ "condense_ref_vals_array", "ui__front__PropControl__lab7_8py.html#aa409eee4195f19f5918ad68315fc04e8", null ],
    [ "getInput", "ui__front__PropControl__lab7_8py.html#aefe1632f0c8f6a91e1cfdaac19fc5493", null ],
    [ "import_ref_vals_array", "ui__front__PropControl__lab7_8py.html#a53aa1d29140b15294fc342f4396b7457", null ],
    [ "isfloat", "ui__front__PropControl__lab7_8py.html#a029e0505519ec770fe272061ec33e0e6", null ],
    [ "plotResponse", "ui__front__PropControl__lab7_8py.html#a58eaf3b8463e3c5f051e4f9e3084c504", null ],
    [ "readLines", "ui__front__PropControl__lab7_8py.html#a99ec25edeb9df256e72a089c1a386e8d", null ],
    [ "saveCSV", "ui__front__PropControl__lab7_8py.html#a2e1480f7964f3822af88449cd697e43f", null ],
    [ "send_ref_vals_array", "ui__front__PropControl__lab7_8py.html#a306a1517d45f2ad69c18dfc8875a5b23", null ],
    [ "array_length", "ui__front__PropControl__lab7_8py.html#acad072d3b4fa4f2c0fdf8eb57654cb58", null ],
    [ "collect_data_front", "ui__front__PropControl__lab7_8py.html#acce6d20f5763f19e393b7c993e0b08f4", null ],
    [ "collect_interrupt", "ui__front__PropControl__lab7_8py.html#a71d80e9e8aeecbcb97a90f2e383963a9", null ],
    [ "J", "ui__front__PropControl__lab7_8py.html#a18a8d5b99567ac40caab249c6af8220e", null ],
    [ "Kp", "ui__front__PropControl__lab7_8py.html#a168401d723ff997f9fa7a184b6281342", null ],
    [ "position_array", "ui__front__PropControl__lab7_8py.html#aafb9c0997424583afd56b6e7f1c7857d", null ],
    [ "readString", "ui__front__PropControl__lab7_8py.html#a11307fde395ece977ccefc44890acba3", null ],
    [ "ser", "ui__front__PropControl__lab7_8py.html#a5b9beba650032c2b1d1c3139b0d8b67a", null ],
    [ "time_array", "ui__front__PropControl__lab7_8py.html#aa2e995133c770352042ce485a1c46b37", null ],
    [ "tx_size", "ui__front__PropControl__lab7_8py.html#aa6b0478326f77a5cba03c82cc3bdd459", null ],
    [ "userInput", "ui__front__PropControl__lab7_8py.html#a46eb0aa52b958568fced83dafc7e7ce9", null ],
    [ "velocity_array", "ui__front__PropControl__lab7_8py.html#ab1b13d9833cb20131dcfab4f84b66447", null ]
];