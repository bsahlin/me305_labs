var shares7_8py =
[
    [ "collect", "shares7_8py.html#a81f241dbebd4b25e25c60aa25696a500", null ],
    [ "condensed_tRefArray", "shares7_8py.html#a48389a6467863cd8da51305d4e1bd5c8", null ],
    [ "condensed_vRefArray", "shares7_8py.html#a569b99d188d12b74839414945c05680c", null ],
    [ "encoder_delta", "shares7_8py.html#a220158c219266c36d41931b736641f9c", null ],
    [ "encoder_position", "shares7_8py.html#a85755389f421dd4ed8e0f792afb4f1cf", null ],
    [ "encoder_position_array", "shares7_8py.html#aad85df1827d742fd123564c94e6341f9", null ],
    [ "encoder_velocity_array", "shares7_8py.html#a01530f5c274d4ae7d3186d0b82f95a21", null ],
    [ "encoder_zeroing", "shares7_8py.html#af76f3f71c4a7821326f8f524372da3dc", null ],
    [ "Kp", "shares7_8py.html#a3ba307a5e5ee8f63fd4080e7808fd483", null ],
    [ "time_array", "shares7_8py.html#acb928b0c8e7b1212d31484e76aa3c511", null ],
    [ "Vdc", "shares7_8py.html#a5f5032216189623408ea8d6f4d559ca1", null ]
];