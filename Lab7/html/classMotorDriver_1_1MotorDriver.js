var classMotorDriver_1_1MotorDriver =
[
    [ "__init__", "classMotorDriver_1_1MotorDriver.html#a5e6e0c9acaa8944bf18e7ecf85d82c7f", null ],
    [ "debugprint", "classMotorDriver_1_1MotorDriver.html#aa94a6c7e6f3aa24065f82e2e7df6d19d", null ],
    [ "disable", "classMotorDriver_1_1MotorDriver.html#abb9a67928c8ed29dd06b04727813411a", null ],
    [ "enable", "classMotorDriver_1_1MotorDriver.html#a296e591519f90c295ca618e961baa1a7", null ],
    [ "mapPinChannel", "classMotorDriver_1_1MotorDriver.html#a7dbc91f20c32bf8ac2f6837659acfa05", null ],
    [ "setDuty", "classMotorDriver_1_1MotorDriver.html#a3eaeeaffd14cf7fa65d433e979dd8057", null ],
    [ "debug", "classMotorDriver_1_1MotorDriver.html#a069e74a3d8c86f7a3fb1505bbf3fb2f2", null ],
    [ "duty", "classMotorDriver_1_1MotorDriver.html#ac5b1e7813ea1d7fd4a71ca24d5515f36", null ],
    [ "IN1_pin", "classMotorDriver_1_1MotorDriver.html#a7fe6850168920ddc25f38c95899fe945", null ],
    [ "IN2_pin", "classMotorDriver_1_1MotorDriver.html#a97bda61202526af5f34138adc5baf7f9", null ],
    [ "nSLEEP_pin", "classMotorDriver_1_1MotorDriver.html#a23e6a5c19063b2d8ab0d5aa205f7ee94", null ],
    [ "tch1", "classMotorDriver_1_1MotorDriver.html#a92ad7e41437a740db6806a2f001f9c85", null ],
    [ "tch2", "classMotorDriver_1_1MotorDriver.html#a66b8f7c7ed21687dbd35df2e6db8539f", null ],
    [ "timer", "classMotorDriver_1_1MotorDriver.html#ab01a28fc3b6e0720c1d9922ac16a4010", null ]
];