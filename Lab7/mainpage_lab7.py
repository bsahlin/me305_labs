# -*- coding: utf-8 -*-
'''
@file mainpage_Lab7.py

@page page_Lab7 Lab 07
    @section sec_Lab7_intro Introduction
        Lab 07 built off the previous lab which introduced closed loop 
        proportional control of a motor. This lab added reference profiles 
        for velocity and position, rather than just looking at the step 
        response. Additionally, a performance metric J was calculated to 
        quantify how well the response plot matched the reference plot.
    @section sec_Lab7_PWMdiagram Pulse Width Modulation Scheme
        The motor in this lab is controlled using pulse width modulation. I 
        continued to use the same PWM scheme as I did for the previous lab, which 
        pulses only one pin at a time, setting the other pin to low. Then, to 
        drive the motor in the opposite direction, which is signified 
        when the input is negative, then the functions of the two pins are swapped.
    @section sec_Lab7_TaskDiagram Task Diagram
        The task diagram for this lab is shown below.
        @image html Lab7TaskDiagram.png
    @section sec_Lab7_FSMs Finite State Machine Diagrams
        The finite state machines are included below. The closed loop controller 
        finite state machine looks like this:
        @image html CLControllerFSM.png
        And the backend finite state machine looks like this:
        @image html BackEndFSM7.png
    @section sec_Lab7_Responses Response Plot
        The best response plot that I got was with a Kp of 0.084. The figure below 
        shows the overlaid reference and response plots for that run as well as 
        the calculated performance metric J. The response plot does not match the 
        reference plot very closely. This is likely partially due to the friction 
        on the motor which makes it difficult to run at the low rpm values specified 
        by the reference plot. To reduce the error, one method could be to switch 
        to using a more complicated controller such as a PID controller, which 
        if designed correctly would reduce both the transient and steady state
        error.
        @image html ResponseJ.png
    @section sec_Lab7_source Source Code
        The source code for the lab assignment can be found in this BitBucket 
        repository:
        [<b>Lab 07 Repository</b>](https://bitbucket.org/bsahlin/me305_labs/src/master/Lab7/lab7main.py)
    @section sec_lab6_files Files
        @ref lab7main.py \n    
        @ref MotorDriver.py \n
        @ref EncoderDriver.py \n
        @ref PropController.py \n
        @ref FSM_BackEnd.py \n
        @ref FSM_CLController.py \n
        @ref shares7.py \n
        @ref ui_front_PropControl_lab7.py \n
'''
