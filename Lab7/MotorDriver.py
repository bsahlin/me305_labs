# -*- coding: utf-8 -*-
'''
@file       MotorDriver.py
@brief      Contains MotorDriver class for PWM motor control
@details    Used for motor control with Nucleo L476RG board along with H-Bridge 
            chip.

@author     Ben Sahlin
@date       November 18, 2020
'''

import pyb

class MotorDriver:
    '''
    @brief      This class implements a motor driver for the ME 305 board
    @details    This class controls a DC motor using pulse width modulation. 
                Designed to work with Nucleo L476RG board along with an H-Bridge 
                chip.               
    '''
    
    def __init__ (self, nSLEEP_pin, IN1_pin, IN2_pin, timer):
            ''' 
            @brief              Creates motor driver object
            @details            Creates a motor driver by initializing GPIO
                                pins and turning the motor off for safety.
            @param nSLEEP_pin   A pyb.Pin object to use as the enable pin.
            @param IN1_pin      A pyb.Pin object to use as the input to half bridge 1.
            @param IN2_pin      A pyb.Pin object to use as the input to half bridge 2.
            @param timer        A pyb.Timer object to use for PWM generation on
                                IN1_pin and IN2_pin. 
            '''
            self.debug = False
            self.nSLEEP_pin = nSLEEP_pin
            self.IN1_pin = IN1_pin
            self.IN2_pin = IN2_pin
            self.timer = timer
            
            self.tch1 = self.timer.channel(self.mapPinChannel(self.IN1_pin), pyb.Timer.PWM, pin=self.IN1_pin)
            self.tch2 = self.timer.channel(self.mapPinChannel(self.IN2_pin), pyb.Timer.PWM, pin=self.IN2_pin)
            self.nSLEEP_pin.low()
            self.tch1.pulse_width_percent(0)
            self.tch2.pulse_width_percent(0)
            
            print('Motor driver object created on pins: \r\n{:} \r\n{:}'.format(self.IN1_pin, self.IN2_pin))
            
            
    def enable(self):
        '''
        @brief      Enables motor control
        @details    Turns on motor control by setting nSLEEP pin for H-Bridge 
                    chip to high.
        '''
        self.nSLEEP_pin.high()
        print('Enabling Motor')
        
    def disable(self):
        '''
        @brief      Disables motor control
        @details    Turns off motor control by setting nSLEEP pin for H-Bridge 
                    chip to low.
        '''
        #turn off sleep pin
        self.nSLEEP_pin.low()
        print('Disabling Motor')
                
    def setDuty(self, duty):
        '''
        @brief          Sets motor duty cycle.
        @details        Sets motor speed using pulse width modulation (PWM). Uses 
                        PWM scheme that pulses one pin or the other depending on 
                        desired direction.
        @param duty     Integer from -100 to 100 representing desired 
                        duty for the pulse width modulation cycle. Negative 
                        values cause the motor to spin the opposite way.
        '''
        self.duty = duty
        #don't allow higher inputs than abs(100)
        if self.duty >= 100:
            self.duty = 100
        elif self.duty <= -100:
            self.duty = -100
        #set duty
        if self.duty >= 0:
            self.tch1.pulse_width_percent(0)
            self.tch2.pulse_width_percent(self.duty)
        elif self.duty < 0:
            self.tch1.pulse_width_percent(abs(self.duty))
            self.tch2.pulse_width_percent(0)
        self.debugprint('Motor: {:} Spinning at duty {:}'.format(self, self.duty))
            
    def mapPinChannel(self, pin):
        '''
        @brief      Returns channel for specified pin
        @details    Designed with Nucleo L476RG board.
        @param pin  Pin object to map to corresponding channel
        '''
        if pin == pyb.Pin.cpu.B4:
            #Pin B4 corresponds to Timer 3 Channel 1
            return 1
        elif pin == pyb.Pin.cpu.B5:
            #Pin B5 corresponds to Timer 3 Channel 2
            return 2
        elif pin == pyb.Pin.cpu.B0:
            #Pin B0 corresponds to Timer 3 Channel 3
            return 3
        elif pin == pyb.Pin.cpu.B1:
            #Pin B1 corresponds to Timer 3 Channel 4
            return 4
        else:
            print('Unknown channel for specified pin {:}'.format(pin))
            
    def debugprint(self, debugstring):
        '''
        @brief                  Prints string if debug parameter is true
        @details                Used for development purposes
        @param debugstring      String to print
        '''
        if self.debug == True:
            print(debugstring)
        
if __name__ == '__main__':
    #for testing purposes, if this file is run by itself it will run this code
    #test with motor 1: Timer 3 channels 1 and 2, pins B4 and B5
    pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4)
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5)
    pin_IN3 = pyb.Pin(pyb.Pin.cpu.B0)
    pin_IN4 = pyb.Pin(pyb.Pin.cpu.B1)
    
    tim = pyb.Timer(3, freq=20000)
    
    moe1 = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, tim)
    moe2 = MotorDriver(pin_nSLEEP, pin_IN3, pin_IN4, tim)