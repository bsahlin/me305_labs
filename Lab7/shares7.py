# -*- coding: utf-8 -*-
"""

@file shares7.py

@brief      A file containing shared variables for Lab 07

@author     Ben Sahlin
@date       December 2, 2020
"""

import array

encoder_position = None
encoder_delta = None
encoder_zeroing = False
#should data be being collected
collect = False
#Kp value for proportional controller to use, default to 0.1
Kp = 0.1
#Vdc value
Vdc = 3.33

#create encoder position array of integers (degrees)
encoder_position_array = array.array('i')
#create time array of integers
time_array = array.array('i')
#create encoder velocity array of floats
encoder_velocity_array = array.array('f')

#reference arrays
condensed_tRefArray = array.array('f')
condensed_vRefArray = array.array('f')