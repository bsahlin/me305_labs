# -*- coding: utf-8 -*-
'''
@file       lab7main.py
@brief      Main file running on Nucleo board for Lab 07
@details    Creates and manages objects and tasks for Lab 07. Uses 
            Micropython libraries and was designed with Nucleo L476RG board 
            along with ME 305 PCB assembly.

@author     Ben Sahlin
@date       November 27, 2020
'''

from EncoderDriver import EncoderDriver
from MotorDriver import MotorDriver
from FSM_CLController import taskCLController
from PropController import PropController
import pyb
import shares7 as shares
from pyb import USB_VCP as uv
from FSM_BackEnd import taskBackEnd


#encoder setup
timer_number = 4
ch1_pin_name = 'B6'
ch2_pin_name = 'B7'
ch1_number = 1
ch2_number = 2

#output shaft ticks/deg = 1000 cyc/rev * 4 ticks/cyc * 1 rev/360 deg)
ticks_per_deg = 1000*4/360

#motor setup
pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4)
pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5)
motortimer = pyb.Timer(3, freq = 20000)

#Kp' = Kp/Vdc
#placeholder Kp for initializing CLController, replaced by frontend input later
Kp =  0.1/shares.Vdc

#limit motor output to between 35% and 100% (still allows 0%)
sat_min = 35
sat_max = 100

#task intervals in microseconds, run backend task faster than controller task
collect_run_interval = int(40e3)
backend_run_interval = int(collect_run_interval/4)
#placeholder, collect_time later changed on file read
shares.collect_time = int(2e6)

#create virtual comm port object, ME 305 PCB uses id=0
usbv1 = uv(0)
#create other objects
PropController1 = PropController(Kp, sat_min, sat_max)
encoder1 = EncoderDriver(timer_number, ch1_pin_name, ch2_pin_name, ch1_number, ch2_number, ticks_per_deg)
motor1 = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, motortimer)
#create tasks
taskCLController1 = taskCLController(collect_run_interval, shares.collect_time, PropController1, encoder1, motor1)
taskBackEnd1 = taskBackEnd(backend_run_interval, usbv1)

while True:
    taskBackEnd1.run()
    taskCLController1.run()