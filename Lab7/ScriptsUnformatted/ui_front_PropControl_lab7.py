# -*- coding: utf-8 -*-
'''
@file       ui_front_PropControl.py

@brief      Frontend UI file for Lab 06
@details    Communicates through serial port to Nucleo board. Prompts for user 
            inputted proportional controller gain Kp and starts data collection.

@author     Ben Sahlin
@date       November 25, 2020
'''
import serial
import numpy as np
import matplotlib.pyplot as plt
#import csv
#import json
import time

def getInput():
    '''
    @brief      Gets user input from PC command line
    '''
    userInput = input('Spyder: Enter a Kp value between 0.001 and 20 to start data collection or press E to exit\n')
    return userInput

def readLines():
    '''
    @brief      Reads and prints next line from serial port
    '''
    readString = str(ser.readline())
    print(str(readString))    

def plotResponse(time_array, position_array, velocity_array, tRefArray, xRefArray, vRefArray, Kp, J):
    '''
    @brief                  Plots time and postion encoder data
    @details                Uses PyPlot from MatPlotLib to plot data sent from Nucleo board     
    @param time_array       Time array in sec
    @param position_array   Position array in deg
    @param velocity_array   Velocity array in rpm
    @param tRefArray        Time reference array in sec
    @param xRefArray        Position reference array in deg
    @param vRefArray        Velocity reference array in rpm
    @param Kp               Proportional gain used to generate plot
    '''
    #plot velocity
    plt.subplot(2,1,1)
    plt.plot(time_array, velocity_array)
    plt.plot(tRefArray, vRefArray)
    plt.xlim([0,time_array[-1]])
    plt.xlabel('Time (sec)')
    plt.ylabel('Velocity (rpm)')
    plt.legend(['Response', 'Reference'])
    plt.title('Response and Reference Plots for Kp = {:}, J = {:.3e}'.format(Kp, J))
    print('\rSpyder: Velocity data plotted')        
    #plot position
    plt.subplot(2,1,2)
    plt.plot(time_array, position_array)
    plt.plot(tRefArray, xRefArray)
    plt.xlim([0, time_array[-1]])
    plt.xlabel('Time (sec)')
    plt.ylabel('Position (degrees)')
    print('\rSpyder: Position data plotted')
    plt.show()
    
# =============================================================================
# def plotVelocity(time_array, velocity_array, Kp):
#     '''
#     @brief                  Plots time and velocity encoder data
#     @details                Uses PyPlot from MatPlotLib to plot data sent from Nucleo board     
#     @param time_array       Time array in microseconds
# 
#     @param Kp               Proportional gain used to generate plot
#     '''
#     plt.subplot(2,1,1)
#     plt.plot(time_array, velocity_array)
#     plt.xlim([0,time_array[-1]])
#     plt.xlabel('Time (sec)')
#     plt.ylabel('Velocity (rpm)')
#     plt.title('Output Shaft Velocity vs Time for Kp = {:}'.format(Kp))
#     plt.show()
#     print('\rSpyder: Velocity data plotted')    
# =============================================================================
    
def saveCSV(time_array, position_array, filename):
    '''
    @brief                  Saves data as CSV file
    @details                Uses Numpy to save encoder data in a CSV file with 
                            a specified filename   
    @param time_array       Time array in seconds
    @param position_array   Encoder position array in degrees
    '''
    np.savetxt(filename, (time_array, position_array), delimiter = ',')
    print('Spyder: Data saved to '+filename)
    
def isfloat(value):
    '''
    @brief          Checks if input can be cast to a float
    @details        Attempts to cast inputted value to a float, returns True if 
                    it can be cast and False if it can not.
    @param value    variable to attempt to cast to float
    '''
    try:
        float(value)
        return True
    except ValueError:
        return False


# =============================================================================
# def import_reference_values(ref_filename, interval):
#     '''
#     @brief                  Imports reference values
#     @details                Uses csv library to import reference time, position, and velocity
#                             values as a dictionary object.
#     @param ref_filename     Filename for reference values including .csv extension
#     @param interval         Interval in ms to get reference values at
#     '''
#     dRef = dict()
#     with open(ref_filename, 'r') as csvf:
#         i = 0
#         for line in csvf:
#             line_Str = csvf.readline().strip().split(',')
#                 #print(i)
#             if i % interval == 0:
#                 #print(i)
#                 #shares.VRef_array[int(i/40)] = float(line_Str[1])
#                 dRef[float(line_Str[0])] = (float(line_Str[1]), float(line_Str[2]))
#             i += 1
#         csvf.close()   
#     return dRef         
# 
# '''
#     dRef = dict()        
#     with open(ref_filename, 'r') as csvf:
#         reader = csv.reader(csvf)
#         for row in reader:
#             #print(row)
#             #to get first element (time)
#             #print(row[0])
#             #match row elements to dictionary creation
#             #key is time in microseconds, value is tuple of velocity and position
#             dRef[float(row[0])] = (float(row[1]), float(row[2]))
#             #print(dRef) 
#         return dRef  
# '''
# def package_ref_values(dRef):
#     condensed_dRef = dict()
#     prev_val = None
#     for k in sorted(dRef.keys()):
#         current_val = dRef[k][0]
#         if prev_val == None or current_val != prev_val:
#             condensed_dRef[k] = dRef[k][0]
#         prev_val = current_val  
#     return condensed_dRef   
# 
# def send_dict(ser, dRef):
#     '''
#     @brief      Sends reference dictionary
#     @details    First sends dictionary length, then Iterates through dictionary and sends each entry as separate JSON string
#     @param ser  Serial object to send on
#     @param dRef Dictionary to send    
#     '''
#     ser.write('Spyder: Sending dictionary of length\r\n'.encode('ascii'))
#     ser.write('{:}\r\n'.format(len(dRef)).encode('ascii'))
#     for key in dRef:
#         print('Frontend Here: {:},{:}\r\n'.format(key,dRef[key][0]).encode('ascii'))
#         ser.write('{:},{:}\r\n'.format(key,dRef[key][0]).encode('ascii'))
#         '''
#         #create dictionary of just one entry
#         entrydict = {key:dRef[key]}        
#         entryjsonstring = json.dumps(entrydict)
#         #print('Spyder: sent {:}'.format(entryjsonstring))
#         ser.write('{:}\r\n'.format(entryjsonstring).encode('ascii'))
#         '''
#     ser.write('Spyder: Dictionary sent\r\n'.encode('ascii'))
#     
# def send_dict_entries(ser, dRef):
#     for key in dRef:
#         time.sleep(.03)
#         #create dictionary of just one entry
#         entrydict = {key:dRef[key]}        
#         entryjsonstring = json.dumps(entrydict)
#         #print('Spyder: sent {:}'.format(entryjsonstring))
#         #print('Spyder sent: {:}\r\n'.format(entryjsonstring).encode('ascii'))
#         ser.write('{:}\r\n'.format(entryjsonstring).encode('ascii'))    
# 
# def send_json(ser, dRef):
#     json_str = json.dumps(dRef)
#     print('Spyder: byte array is:')
#     print('{:}\n'.format(json_str).encode('ascii'))
#     ser.write('{:}\n'.format(json_str).encode('ascii'))           
# 
# =============================================================================

def import_ref_vals_array(ref_filename, interval):
    print('Spyder: Importing reference values')
    tRefArray = np.array
    vRefArray = np.array
    xRefArray = np.array
    with open(ref_filename, 'r') as csvf:
        i = 0
        for line in csvf:
            line_Str = line.strip().split(',')#csvf.readline().strip().split(',')
                #print(i)
            if i % interval == 0:
                #print(i)
                #shares.VRef_array[int(i/40)] = float(line_Str[1])
                #dRef[float(line_Str[0])] = (float(line_Str[1]), float(line_Str[2]))
                tRefArray = np.append(tRefArray, float(line_Str[0]))
                vRefArray = np.append(vRefArray, float(line_Str[1]))
                xRefArray = np.append(xRefArray, float(line_Str[2]))
            i += 1
        csvf.close()   
    #numpy array has array type as first element
    tRefArray = np.delete(tRefArray,0)
    vRefArray = np.delete(vRefArray,0)
    xRefArray = np.delete(xRefArray,0)
    print('Spyder: Reference values imported')
    return tRefArray, vRefArray, xRefArray

def condense_ref_vals_array(tRefArray, vRefArray):
    print('Spyder: Condensing reference values')
    #note that this gives values like 4.004 (for 40ms interval) for when the velocity changes because
    #in the reference file they change not at 4.000 but at 4.001
    prev_val = None
    condensed_vRefArray = np.array
    condensed_tRefArray = np.array
    for i in range(len(vRefArray)):
        current_val = vRefArray[i]
        current_time = tRefArray[i]
        if prev_val == None or current_val != prev_val:
            condensed_vRefArray = np.append(condensed_vRefArray, current_val)
            condensed_tRefArray = np.append(condensed_tRefArray, current_time)
        prev_val = current_val
    #also include last value from non condensed array
    condensed_tRefArray = np.append(condensed_tRefArray, current_time)
    condensed_vRefArray = np.append(condensed_vRefArray, current_val)
    #get rid of garbage first values
    condensed_tRefArray = np.delete(condensed_tRefArray, 0)
    condensed_vRefArray = np.delete(condensed_vRefArray, 0)
    #print('Condensed velocity to {:}'.format(condensed_vRefArray))
    #print('Condensed time to {:}'.format(condensed_tRefArray))
    print('Spyder: Reference values condensed')
    return (condensed_tRefArray, condensed_vRefArray)

def send_ref_vals_array(ser, tRefArray, vRefArray):
    #include required interval as parameter
    print('Spyder: Sending ref values')
    ser.write('Sending ref vals with length\r\n'.encode('ascii'))
    ser.write('{:}\r\n'.format(len(vRefArray)).encode('ascii'))
    for i in range(len(vRefArray)):
        #have to slow down sending to not overflow buffer
        time.sleep(.05)
        #print('Spyder sent: {:},{:}\r\n'.format(tRefArray[i],vRefArray[i]).encode('ascii'))
        ser.write('{:},{:}\r\n'.format(tRefArray[i],vRefArray[i]).encode('ascii'))
    ser.write('Ref vals sent\r\n'.encode('ascii'))

def calc_perf_metric(tArray, vArray, xArray, vRef, xRef):
    '''
    @brief          Calculates performance metric K
    @details        Uses discretized equation to calculate K for a data collection run
    @param tArray   Measured time array in sec
    @param vArray   Measured velocity array in rpm
    @param xArray   Measured position array in deg
    @param vRef     Reference velocity array in rpm
    @param xRef     Reference position array in deg
    @return J       Calculated performance metric
    '''
    K = len(tArray)
    J = 0
    for i in range(K):
        #assumes that the vArray time values are close enough to the times when the runs should have occurred
        #also know that the reference arrays have an extra value because they have one at the end of the run time
        #so no issue with indexing since we're indexing through the measured array
        J = J + (vRef[i]-vArray[i])**2 + (xRef[i]-xArray[i])**2
    J = J/K
    J = round(J)
    print('Spyder: Perf metric J = {}'.format(J))
    return J
    
#get reference values from file, this interval determines data resolution
(tRefArray, vRefArray, xRefArray) = import_ref_vals_array('reference.csv', 40)
(condensed_tRefArray, condensed_vRefArray) = condense_ref_vals_array(tRefArray, vRefArray)

while True:
    collect_data_front = False
    userInput = getInput()
    ser = serial.Serial(port = 'COM4', baudrate = 9600, timeout=1)#115273, timeout=10)
    ser.set_buffer_size(tx_size=2**31-1)
    if userInput == 'E':
        ser.close()
        break
    #check if input is valid
    elif(isfloat(userInput) == True):
        if float(userInput) >= 0.001 and float(userInput) <= 20:
            Kp = userInput
            #send Kp value
            ser.write('Sending Kp\r\n'.encode('ascii'))
            ser.write('{:}\r\n'.format(userInput).encode('ascii'))
            #send condensed arrays
            send_ref_vals_array(ser, condensed_tRefArray, condensed_vRefArray)
            #start data collection
            collect_data_front = True
            collect_interrupt = False
        else:
            print('Spyder: Input is not between 0.1 and 20')
            
    elif(isfloat(userInput) == False):
        print('Spyder: Input is not a valid Kp')
    else:
        print('Spyder: Invalid input')
            
    while collect_data_front == True:
        #print(ser.in_waiting)
        if ser.in_waiting > 0:
            #if anything waiting in serial
            readString = str(ser.readline().decode('ascii')).strip()
            #print('Spyder: readstring is '+readString)        
            if readString == 'Data sending with length':
                #if reading says it's sending data
                print('Spyder: Reading data')
                array_length = int(ser.readline())
                #create arrays
                time_array = np.empty(array_length)
                position_array = np.empty(array_length)
                velocity_array = np.empty(array_length)
                print('Spyder: Array length of '+str(array_length))
                for i in range(array_length):
                    [time_array[i], position_array[i], velocity_array[i]] = ser.readline().decode('ascii').strip().split(',')
                    #print('Spyder: Time is: {:}, Position is: {:}, Velocity is {:}'.format(time_array[i], position_array[i], velocity_array[i]))
                #if next readstring is not 'Data sent', there is an error
                readString = str(ser.readline().decode('ascii')).strip()            
                if readString == 'Data sent':
                    print('Nucleo: '+readString)
                    print('Spyder: Data recieved')
                    #convert time array to seconds from microseconds
                    time_array = time_array/1e6
                    #convert velocity array to rpm from deg/s
                    velocity_array = velocity_array/60
                    #calculate K
                    J = calc_perf_metric(time_array, velocity_array, position_array, vRefArray, xRefArray)
                    #plot graphs
                    plotResponse(time_array, position_array, velocity_array, tRefArray, xRefArray, vRefArray, Kp, J)
                    collect_data_front = False
                else:
                    print('Spyder: Data Read Error, readstring is: '+readString)
            #elif just print reading
            else:
                print('Nucleo: '+readString)
    ser.close()