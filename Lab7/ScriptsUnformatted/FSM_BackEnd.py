# -*- coding: utf-8 -*-
'''
@file       FSM_BackEnd.py

@brief      Task for backend management
@details    Includes finite state machine for managing user input and data output 
            through virtual USB connection for Lab 06. Designed using Nucleo L476RG 
            along with ME 305 PCB assembly. USB port on PCB assembly used for 
            PC to Nucleo communication.
            
@author     Ben Sahlin
@date       November 25, 2020
'''
import utime
import shares7 as shares
from array import array

class taskBackEnd:
    #Define states
    S0_Init         = 0
    S1_WaitForCmd   = 1
    S2_WaitForData  = 2
    S3_SendData     = 3
    
    def __init__(self, run_interval, USB_VCP):
        '''
        '''
        self.debug = True
        self.run_interval = run_interval
        self.usbv = USB_VCP
        self.debugprint('taskBackEnd object created with {:} run interval and {:} uart object'.format(self.run_interval, self.usbv))
        self.current_time = utime.ticks_us()
        self.next_time = utime.ticks_add(self.current_time, self.run_interval)
        self.state = self.S0_Init
        
    def run(self):
        if(self.state == self.S0_Init):
            #state 0
            self.transitionTo(self.S1_WaitForCmd)
        elif(self.state == self.S1_WaitForCmd):
            #state 1
            if self.usbv.any() >=1:
                self.inputstring = self.usbv.readline().decode('ascii').strip()
                if self.inputstring == 'Sending Kp':
                    #check for valid input- checked in frontend
                    self.inputKp = self.usbv.readline().decode('ascii').strip()
                    self.inputKp = float(self.inputKp)
                    if self.inputKp <= 20.0 and self.inputKp >= 0.001:
                    #Kp is a valid choice
                        self.inputKp = self.inputKp/shares.Vdc
                        self.debugprint('Setting shares Kp to {:}'.format(self.inputKp))
                        shares.Kp = self.inputKp
                elif self.inputstring == 'Sending ref vals with length':
                    self.reflength = int(self.usbv.readline().decode('ascii').strip())
                    #clear arrays
                    shares.condensed_tRefArray = array('f')
                    shares.condensed_vRefArray = array('f')
                elif self.inputstring == 'Ref vals sent':
                    #all values sent, check length
                    if len(shares.condensed_vRefArray) == self.reflength:
                        #data send successful
                        #start data collection in CLController by setting shares.collect to True
                        shares.collect = True
                        self.usbv.write('Starting data collection\r\n'.encode('ascii'))
                        self.transitionTo(self.S2_WaitForData)
                    else:
                        print('Data transmission error')
                else:
                    #otherwise condensed time and velocity reference arrays sending from frontend
                    (t,v)= self.inputstring.split(',')
                    shares.condensed_tRefArray.append(float(t))
                    shares.condensed_vRefArray.append(float(v))
                
                '''
                #also get json string which comes next from frontend
                self.dRef = self.recieve_dict()
                
                self.jsonstring = self.usbv.readline().decode('ascii').strip()
                self.inputjson = ujson.loads(self.jsonstring)
                #gives dict object with string key and integer tuple
                #change to int keys but keep integer tuples same
                self.dRef = {float(k):v for k,v in self.inputjson.items()}
                #now adjust units to microseconds, deg/s, and keep as deg
                self.dRef = {int(k*1e6):[v[0]*360/60, v[1]] for k,v in self.dRef.items()}
                #test dict object
                self.debugprint('At key {:}, value {:}'.format(0.009e6, self.dRef[int(0.009e6)]))
                self.usbv.write('Nucleo: json recieved\r\n')
                '''



        elif(self.state == self.S2_WaitForData):
            #state 2
            #wait for shares.collect to be set to false by CLController
            if shares.collect == False:
                self.usbv.write('Data collection finished\r\n'.encode('ascii'))
                self.transitionTo(self.S3_SendData)
                
        elif(self.state == self.S3_SendData):
            #state 3
            #check array lengths to make sure they match
            if len(shares.time_array) == len(shares.encoder_position_array) and len(shares.time_array) == len(shares.encoder_velocity_array):                                 
                self.usbv.write('System collected {:} data points\n'.format(len(shares.time_array)))
                self.usbv.write('Data sending with length\r\n')
                self.usbv.write('{:}\r\n'.format(len(shares.time_array)))
                #send tuples of (time, position, velocity)
                for i in range(len(shares.time_array)):
                    self.usbv.write('{:},{:},{:}\r\n'.format(shares.time_array[i], shares.encoder_position_array[i], shares.encoder_velocity_array[i]))
                self.usbv.write('Data sent\r\n')
            else:
                print('System data collection error')
            self.transitionTo(self.S1_WaitForCmd)
        else:
            print('Invalid state code')
            
    def transitionTo(self, next_state):
        '''
        @brief              Sets taskUI state to specified next state
        @param next_state   Variable representing the next state for the FSM
        '''
        self.state = next_state
        self.debugprint('BackEnd Switch to state '+ str(next_state))      
        
    def debugprint(self, debugstring):
        '''
        @brief                  Prints string if debug parameter is true
        @details                Used for development purposes
        @param debugstring      String to print
        '''
        if self.debug == True:
            print(debugstring)
        
'''           
    def recieve_dict(self):
        self.readstring = self.usbv.readline().decode('ascii').strip()
        print(self.readstring)
        if self.readstring == 'Spyder: Sending dictionary of length':
            #frontend sending dictionary
            #next send is dict length
            self.dlength = self.usbv.readline().decode('ascii').strip()
            self.dlength = int(self.dlength)
            self.usbv.write('Nucleo: Recieving dict of length {:}\r\n'.format(self.dlength).encode('ascii'))
            #now it sends lines of dict entries
            i = 0
            self.dRef = dict()

            for i in range(self.dlength):
                utime.sleep(.1)
                #recieve dict entry, add to dRef
                #if the frontend doesn't send fast enough this won't work
                self.readstring = self.usbv.readline().decode('ascii').strip()
                self.readtuple = self.readstring.split(',')
                print('readtuple is {:}'.format(self.readtuple))
                self.dictentry = {self.readtuple[0]:[self.readtuple[1]]}
                
                
                
                try:
                    self.dictentry = ujson.loads(self.readstring)
                except ValueError:
                    self.usbv.write('JSON value error with readstring {:}'.format(self.readstring).encode('ascii'))
                
                #print(self.dictentry)
                #add dict entry to dRef
                self.dRef.update(self.dictentry)
            #now check to make sure frontend is done
            self.readstring = self.usbv.readline().decode('ascii').strip()
            if self.readstring == 'Spyder: Dictionary sent':
                #dictionary send was successful
                return self.dRef
            else:
                self.debugprint('Nucleo: Invalid dictionary send process')
        else:
            #frontend not sending dict
            self.debugprint('Frontend not sending dict')
        
'''      
            

        
        