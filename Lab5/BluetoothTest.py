# -*- coding: utf-8 -*-
"""
Created on Sat Nov  7 10:36:48 2020

@author: ben
"""
from machine import UART
import pyb

#create bluetooth uart object
uart = UART(3, 9600)
#create LED pin object and start with it on
pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
pinA5.high()

while True:
    if uart.any() != 0:
        val = uart.readline()
        if val == bytes('\x00','ascii'):#b'\x00':
            print(val,' turns it OFF')
            pinA5.low()
        elif val == b'\x01':
            print(val,' turns it ON')
            pinA5.high()


'''
while True:
    if uart.any() != 0:
        userinput = int(uart.read())
        #print(str(userinput))
        #userinput = int(userinput)
        if userinput == 0:
            #turn off LED
            print(str(userinput) + ' turns LED off')
            pinA5.low()
        elif userinput == 1:
            #turn on LED
            print(str(userinput) + ' turns LED on')
            pinA5.high()
'''