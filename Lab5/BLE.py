# -*- coding: utf-8 -*-
'''
@file           BLE.py
@brief          Bluetooth LED object class
@details        Includes Bluetooth LED (BLE) class that defines functions for 
                controlling the LED as well as reading and writing from the 
                specified UART object.
@author         Ben Sahlin
@date           November 11, 2020
'''
import pyb
from machine import UART

class BLE:
    
    def __init__ (self, pin_name, uart_number):
        '''
        @brief              Creates a BLE object
        @details            Creates BLE object to control a specified LED over a 
                            specified UART number. Sets up UART and pin objects and 
                            initial LED state.
        @param pin_name     String representing pin to use for LED control
        @param uart_number  Variable representing which UART number to use
        '''
        self.pin_name = pin_name        
        self.pin = pyb.Pin(self.pinMapper(self.pin_name), pyb.Pin.OUT_PP)
        self.uart_number = uart_number
        self.uart = UART(self.uart_number, 9600)
        #start with LED off
        self.off()
        self.is_on = False
        print('BLE object created on pin {:} with UART {:}'.format(self.pin_name, self.uart_number))
        
    def off(self):
        '''
        @brief Sets LED off
        '''
        self.pin.low()
        self.is_on = False
        
    def on(self):
        '''
        @brief Sets LED on
        '''
        self.pin.high()
        self.is_on = True

    def swap(self):
        '''
        @brief Swaps state of LED on/off
        '''
        if self.is_on == True:
            self.off()
        elif self.is_on == False:
            self.on()
                
    def read_input(self):
        '''
        @brief Reads input from UART
        '''
        self.input = self.uart.read()
        return self.input
    
    def write(self, message):
        '''
        @brief          Writes message to UART
        @param message  String representing message to send
        '''
        self.uart.write(message)
        
    def pinMapper(self, pin_name):
        '''
        @brief              Returns corresponding pin object for given pin name
        @details            Specifically for Nucleo L476RG, returns pin object 
                            for the inputted pin name string.
        @param pin_name     String representing pin name to map
        '''
        if pin_name == 'A5':
            return pyb.Pin.cpu.A5