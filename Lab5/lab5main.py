# -*- coding: utf-8 -*-
'''
@file       lab5main.py
@brief      Main file running on Nucleo board for Lab 05
@details    Creates and manages objects and tasks for Lab 05, runs the state 
            machine task at a specified interval.
@author     Ben Sahlin
@date       November 11, 2020
'''

from BLE import BLE
from FSM_LEDControl import taskLEDControl

run_interval = 100 #microseconds
BLE1 = BLE('A5',3)

taskBlinkLED1 = taskLEDControl(run_interval, BLE1)

while True:
    taskBlinkLED1.run()