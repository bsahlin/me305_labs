# -*- coding: utf-8 -*-
'''
@file mainpage_lab5.py

@page page_Lab5 Lab 05
    @section sec_Lab5_intro Introduction
        Lab 5 builds on the concepts in Lab 4 with user control of a task. In 
        this lab, the input comes from a phone app and is sent to a bluetooth 
        antenna connected to the Nucleo board. The user is able to control the 
        frequency of a blinking LED on the Nucleo board through the app UI.
    @section sec_Lab05_FSMdiagram FSM Diagram
        The finite state machine diagram for the Bluetooth LED control is shown below. 
        @image html BLEDControl_fsmdiagram.png
    @section sec_Lab05_app iPhone App
        The accompanying app for this lab was built using Thunkable. Screenshots 
        of the Thunkable block style code and the UI are shown below.
        @image html ThunkBlocksLab5.PNG
        @image html ThunkUIlab5.png
    @section sec_Lab05_taskdiagram Task Diagram
        The task diagram for this lab is shown below.
        @image html TaskDiagramLab5.png
    @section sec_Lab05_source Source Code
        The source code for the lab assignment can be found in this BitBucket 
        repository: 
        [<b>Lab 05 Repository</b>](https://bitbucket.org/bsahlin/me305_labs/src/master/Lab5\lab5main.py)

    @section sec_Lab05_files Files
        @ref lab5main.py \n
        @ref FSM_LEDControl.py \n
        @ref BLE.py \n

'''