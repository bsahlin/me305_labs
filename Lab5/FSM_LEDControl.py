# -*- coding: utf-8 -*-
'''
@file           FSM_LEDControl.py

@brief          Task for Bluetooth LED control in Lab 05
@details        Includes finite state machine for running Bluetooth LED task. 
                Allows for blinking the specified LED at integer rates between 
                0 and 10 Hz.
@author         Ben Sahlin
@date           November 11, 2020
'''

import utime
from machine import UART
from BLE import BLE

class taskLEDControl:
    ## Constant defining State 0 - Initialization
    S0_Init = 0
    ## Constant defining State 1 - Waiting for Char
    S1_Wait_For_Char = 1
    ## Constant defining State 2 - Execute Command
    S2_Exectute_Command = 2  
    
    def __init__(self, run_interval, BLE):
        '''
        @brief                  Creates a taskLEDControl object
        @param run_interval     Time between runs in microseconds
        @details                Initializes LED Control task with start time, 
                                next time, UART object, and state
        '''
        print('taskLEDControl object created')
        self.run_interval = run_interval
        self.BLE = BLE
        self.state = self.S0_Init
        self.start_time = utime.ticks_us()
        self.next_time = utime.ticks_add(self.start_time, self.run_interval)
        #set next blink time to be never
        self.next_blink_time = -1
        #Toggle optional print statements for debugging purposes
        self.debug = False
        
    def run(self):
        '''
        @brief      runs one iteration of the task
        '''
        self.current_time = utime.ticks_us()
        if (utime.ticks_diff(self.current_time, self.next_time)) > 0:
            if (self.state == self.S0_Init):
                #state 0
                self.printCommands()
                self.transitionTo(self.S1_Wait_For_Char)
                #start with invalid blink interval
                self.blink_interval = -1
                
            elif(self.state == self.S1_Wait_For_Char):
                #state 1
                if (self.BLE.uart.any() != 0 ):
                    self.input = self.BLE.read_input()
                    self.transitionTo(self.S2_Exectute_Command)
                if utime.ticks_diff(self.current_time, self.next_blink_time) > 0 and self.blink_interval != -1:
                    #swap LED on/off
                    self.BLE.swap()
                    #set next blink time
                    self.next_blink_time = utime.ticks_add(self.current_time, self.blink_interval)

            elif(self.state == self.S2_Exectute_Command):
                #run state 2 code
                if self.input.isdigit() == True:
                    self.input = int(self.input)
                    if self.input <= 10 and self.input > 0:
                        #blink interval is 1e6 microsec/sec * cyc/(2 swap) * sec/cyc
                        self.blink_interval = int(1e6/2/self.input)
                        self.debugprint(self.blink_interval)
                        print('User LED blinking at {:} Hz'.format(self.input))
                        self.BLE.swap()
                        self.next_blink_time = utime.ticks_add(self.current_time, self.blink_interval)
                    else:
                        print('Input not an integer between 0 and 10 Hz\n')
                        self.printCommands()
                else:
                    print('Invalid command\n')
                    self.printCommands()
                self.transitionTo(self.S1_Wait_For_Char)

            else:
                #invalid state code
                print('Invalid state')
            self.next_time = utime.ticks_add(self.current_time, self.run_interval)
            
    def transitionTo(self, next_state):
        '''
        @brief              Sets taskUI state to specified next state
        @param next_state   Variable representing the next state for the FSM
        '''
        self.state = next_state
        self.debugprint('switch to '+ str(next_state))
    
    def printCommands(self):
        '''
        @brief      Prints available LED commands
        '''
        print('''Available Commands:
              \r'1-10' Blink user LED at specified integer frequency (Hz)
              ''')
              
    def debugprint(self, debugstring):
        '''
        @brief                  Prints string if debug parameter is true
        @details                Used for development purposes
        @param debugstring      String to print
        '''
        if self.debug == True:
            print(debugstring)
            

          


      
          
          
          