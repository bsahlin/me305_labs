'''
@file mainpage.py

@mainpage
    @section sec_labsList List of Labs
        @ref page_lab01 Fibonnaci Sequence

@page page_lab01 Lab 01
    @section sec_lab1_intro Introduction
        Lab 01 looks at creating algorithms, in this case for the fibonacci 
        sequence. The goal of the lab was to create a function for inputting a 
        specified index and returning the fibonacci number corresponding to
        that index. The function must also use a more efficient approach than
        the top-down approach. I used a bottom-up approach for this lab.
        
    @section sec_Lab1_source Source Code
        The source code for the lab can be found in this BitBucket repository:
        [<b>Lab 01 Repository</b>](https://bitbucket.org/bsahlin/me305_labs/src/master/Lab1/)</b>
        
    @section sec_lab1_files Files
        @ref fibonacci.py
'''


