'''
@file fibonacci.py

@brief  Calculates fibonacci number for an inputted index

Contains a function that returns the fibonacci number when given 
the desired index. Prompts the user for an index when the file is
run by itself and returns the fibonnaci number for that index. It will 
continue to prompt the user until they select that they are done.
            
@author Ben Sahlin

@date September 24, 2020

'''

import sys

def fib (idx) :
    ''' This calculates a Fibonacci number corresponding to a specified index
    @param idx An integer specifying the index of the desired Fibonacci number'''
    
    if idx.isdigit() == True :
        idx = int(idx)
    
        print ('Calculating Fibonacci number at '
               'index n = {:}.'.format(idx))
        fiblist = [0,1]         #set first two fibonacci values of 0 and 1
        i = 2                   #set index counter to 2
        
        while i <= idx:
            fiblist.append(fiblist[i-2] + fiblist[i-1])
            #print(fiblist[i])
            i += 1
        
        print('The fibonacci number for index ' + str(idx) + ' is ' + str(fiblist[idx])+'\n')
        
    else:
        print('Index value of ' + idx + ' is not a valid integer index\n')
    

    
    
if __name__ == '__main__':
    ''' The lines in this if statement only run when the script is run 
    standalone, it won't run if imported as a module'''
    in_use = True
    ## @param in_use A boolean determining if the program is being used or not
    
    while in_use == True :
        print('This program finds the Fibonacci number corresponding to an entered index.')
        input_idx = input('Enter the desired index or enter "stop" to exit program: ')
        if input_idx == 'stop':
            sys.exit()
        else:
            fib(input_idx)
            
    