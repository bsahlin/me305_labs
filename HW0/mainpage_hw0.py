# -*- coding: utf-8 -*-
'''
@file mainpage_hw0.py

@page page_HW00 HW 00
    @section sec_HW00_intro Introduction
        HW 00 looks at creating finite state machines (FSM). For this 
        assignment the system was an elevator with two floors. There are four 
        buttons total, two inside to select the desired floor and two that are 
        triggered when the elevator is on Floor 1 and Floor 2 respectively. 
        There is also a motor which can spin forwards, backwards, and stop.
        
    @section sec_HW00_FSMdiagram FSM Diagram
        The finite state machine diagram for each elevator looks like this: 
        @image html fsmdiagram.png

    @section sec_HW00_source Source Code
        The source code for the homework assignment can be found in this BitBucket 
        repository: 
        [<b>HW 00 Repository</b>](https://bitbucket.org/bsahlin/me305_labs/src/master/HW0/main_hw0.py)</b>

    @section sec_HW00_files Files
        @ref main_hw0.py \n
        @ref FSM_Elevator.py \n

'''
