# -*- coding: utf-8 -*-
'''
@file       main_hw0.py
@brief      Main file for HW 00
@details    Creates objects and tasks to manage and run two elevator tasks 
@author     Ben Sahlin
@date       October 3, 2020
'''

from FSM_Elevator import Button, MotorDriver, TaskElevator

# Create objects to pass into first elevator task
first_0 = Button('P00')
second_0 = Button('P01')
button_1_0 = Button('P02')
button_2_0 = Button('P03')
Motor_0 = MotorDriver('Motor 0')

# Create first elevator task
elevator0 = TaskElevator(0.01, button_1_0, button_2_0, first_0, second_0, Motor_0, 'Elevator 0')

# Create objects to pass into first elevator task
first_1 = Button('P10')
second_1 = Button('P11')
button_1_1 = Button('P12')
button_2_1 = Button('P13')
Motor_1 = MotorDriver('Motor 1')

# Create second elevator task
elevator1 = TaskElevator(0.01, button_1_1, button_2_1, first_1, second_1, Motor_1, 'Elevator 1')

# Run tasks
for N in range(1000000):
    elevator0.run()
    elevator1.run()