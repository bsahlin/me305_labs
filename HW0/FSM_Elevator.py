# -*- coding: utf-8 -*-
'''
@file       FSM_Elevator.py
     
@brief      Classes for tasks and objects in HW 00- Finite State Machines. 
@details    Includes elevator task, button class, and motor driver class. Note 
            that as of right now the elevator will just stay at the floor it 
            goes to until someone gets in and presses the button to switch 
            floors since there are no simulated elevator call buttons outside 
            of the elevator on each floor.

@author     Ben Sahlin

@date       October 3, 2020

'''

import time
from random import choice


class TaskElevator:
    '''
    @brief      A finite state machine to control a two floor elevator
    @details    This class implements a FSM to control the operation of a 
                two floor elevator which has two interior buttons.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_Init         = 0
    ## Constant defining State 1 - Moving Down
    S1_MovingDown   = 1
    ## Constant defining State 2 - Stopped at Floor 1
    S2_StoppedAt1   = 2
    ## Constant defining State 3 - Moving Up
    S3_MovingUp     = 3
    ## Constant defining State 4 - Stopped at Floor 2
    S4_StoppedAt2   = 4
    
    def __init__(self, interval, button_1, button_2, first, second, Motor, name):
        '''
        @brief              Creates a TaskElevator object
        @param button_1     An object from class Button representing button inside elevator to go to floor 1
        @param button_2     An object from class Button representing button inside elevator to go to floor 2
        @param first        An object from class Button representing whether elevator is at floor 1
        @param second       An object from class Button representing whether elevator is at floor 2
        @param Motor        An object from class MotorDriver representing a DC motor
        '''
        
        ## The state to run on the next iteration of the task
        self.state = self.S0_Init
        ## The button object used for the interior floor 1 button
        self.button_1 = button_1
        ## The button object used for the interior floor 2 button
        self.button_2 = button_2
        ## The button object used for representing whether elevator is at floor 1
        self.first = first
        ## The button object used for representing whether elevator is at floor 2
        self.second = second
        ## The motor object for moving the elevator
        self.Motor = Motor
        ## Counter for the number of times the task has run
        self.runs = 0
        ## Time in seconds between task runs
        self.interval = interval
        ## Timestamp for first iteration
        self.start_time = time.time()
        ## Timestamp for when the task should run next
        self.next_time = self.start_time+self.interval
        ## Name the elevator
        self.name = name
        
        print('Elevator task created with name '+str(self.name))
        
    def run(self):
        '''
        @brief      Runs one task iteration

        '''
        self.current_time = time.time()
        if self.current_time > self.next_time:
            if(self.state == self.S0_Init):
                # Run state 0 code
                self.Motor.Downward()
                self.transitionTo(self.S1_MovingDown)
                print(str(self.runs) + ': State 0 ' + str(self.current_time-self.start_time))
                
            elif(self.state == self.S1_MovingDown):
                # Run state 1 code
                if (self.first.getButtonState() == 1):
                    self.Motor.Stop()
                    self.button_1.setButtonState(0)
                    self.transitionTo(self.S2_StoppedAt1)
                    print(str(self.runs) + ': State 1 ' + str(self.current_time-self.start_time))
                    
            elif(self.state == self.S2_StoppedAt1):
                # Run state 2 code
                if (self.button_2.getButtonState() == 1):
                    self.button_2.setButtonState(0)
                    self.Motor.Upward()
                    self.transitionTo(self.S3_MovingUp)
                    print(str(self.runs) + ': State 2 ' + str(self.current_time-self.start_time))
            
            elif(self.state == self.S3_MovingUp):
                # Run state 3 code
                if (self.second.getButtonState() == 1):
                    self.second.setButtonState(0)
                    self.Motor.Stop()
                    self.transitionTo(self.S4_StoppedAt2)
                    print(str(self.runs) + ': State 3 ' + str(self.current_time-self.start_time))                    
            
            elif(self.state == self.S4_StoppedAt2):
                # Run state 4 code
                if (self.button_1.getButtonState() == 1):
                    self.button_1.setButtonState(0)
                    self.Motor.Downward()
                    self.transitionTo(self.S1_MovingDown)
                    print(str(self.runs) + ': State 4 ' + str(self.current_time-self.start_time))
                
            else:
                # Invalid state code
                print('Invalid state code')
                
            ## Set up variables for next run
            self.runs += 1
            self.next_time += self.interval

    def transitionTo(self, newState):
        '''
        @brief              Updates the variable defining the next state to run
        @param newState     Next state to run
        '''
        self.state = newState
        #print(str(self) + ' state set to: '+str(self.state))
        
class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that can either be pushed or not
    '''
    def __init__(self, pin):
        '''
        @brief          Creates a Button object
        @param pin      A pin object the button is connected to
        @param state    Whether the button is pressed or not
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        self.state = 0  #start with button not pressed
        print('Button object created attached to pin '+ str(self.pin))
    
    def getButtonState(self):
        '''
        @brief  Gets the button state
        '''
        #randomly sets button state since no hardware
        self.setButtonState(choice([0,1]))
        return self.state
    
    def setButtonState(self, new_state):
        '''
        @brief              Sets button state
        @param new_state    New state
        '''
        self.state = new_state
        #print(str(self) + ' button state set to: '+ str(self.state))
        
class MotorDriver:
    '''
    @brief      A motor driver
    @details    This class represents a motor driver used to make the elevator 
                go up and down.
    '''
    def __init__(self, name):
        '''
        @brief          Crates a MotorDriver object
        @param name     Name of the motor
        '''
        self.name = name
        print('Motor object created with name '+str(self.name))
    
    def Upward(self):
        '''
        @brief      Makes the motor run upwards
        '''
        print (str(self.name)+' moving upwards')
        
    def Downward(self):
        '''
        @brief      Makes the motor run downwards
        '''
        print(str(self.name)+' moving downwards')
    
    def Stop(self):
        '''
        @brief      Makes the motor stop
        '''
        print(str(self.name)+' stopped')












