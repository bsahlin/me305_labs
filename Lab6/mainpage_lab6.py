# -*- coding: utf-8 -*-
'''
@file mainpage_Lab6.py

@page page_Lab6 Lab 06
    @section sec_Lab6_intro Introduction
        Lab 6 looks at controlling a motor using pulse width modulation. To do this, 
        the first task of the lab was to create a motor driver class that would 
        allow forward and backward control of a motor using the Nucleo 476RG along 
        with the ME 305 PCB which includes an H-Bridge for motor control. The next 
        part of the lab looked at the closed loop step response of a motor 
        with a user inputted proportional controller gain.
    @section sec_Lab6_PWMdiagram Pulse Width Modulation Scheme
        The motor in this lab is controlled using pulse width modulation. For 
        this lab I choose to use a pulse width modulation scheme which 
        pulses only one pin at a time, setting the other pin to low. Then, to 
        drive the motor in the opposite direction, which is signified 
        when the input is negative, then the functions of the two pins are swapped.
    @section sec_Lab6_TaskDiagram Task Diagram
        The task diagram for this lab is shown below.
        @image html Lab6TaskDiagram.png        
    @section sec_Lab6_FSMs Finite State Machine Diagrams
        The finite state machines are included below. The closed loop controller 
        finite state machine looks like this:
        @image html CLControllerFSM.png
        And the backend finite state machine looks like this:
        @image html BackEndFSM.png
    @section sec_Lab6_StepResponses Step Response Plots
        Step response graphs from this lab for varying proportional controller 
        gains are shown below. I started with an extremely low Kp of .001, with 
        the step response plot shown below:
        @image html Kp0_001.png
        With Kp = .001, there is no response at all because the proportional 
        controller will never output a value that is large enough to make the 
        motor turn. Next, I progressively increased the Kp values, shown below:
        @image html Kp0_01.png
        @image html Kp0_02.png
        @image html Kp1.png
        @image html Kp20.png
        Looking at the plots, the response is unstable for too low of a gain 
        because the motor is not spinning fast enough to produce a stable graph, 
        but then the response improves as we reach a gain of 1. After that, 
        there is no noticeable difference in response by increasing the gain higher 
        than 1.
    @section sec_Lab6_source Source Code
        The source code for the lab assignment can be found in this BitBucket 
        repository:
        [<b>Lab 06 Repository</b>](https://bitbucket.org/bsahlin/me305_labs/src/master/Lab6/lab6main.py)
    @section sec_lab6_files Files
        @ref lab6main.py \n    
        @ref MotorDriver.py \n
        @ref EncoderDriver.py \n
        @ref PropController.py \n
        @ref FSM_BackEnd.py \n
        @ref FSM_CLController.py \n
        @ref shares6.py \n
        @ref ui_front_PropControl.py \n

'''
