# -*- coding: utf-8 -*-
'''
@file       FSM_CLController.py

@brief      Task for closed loop motor control
@details    Includes finite state machine for running closed loop controller 
            task for Lab 06. Uses MotorDriver, EncoderDriver, and controller \
            objects to achieve closed loop motor control.

@author     Ben Sahlin
@date       November 25, 2020
'''

import utime
import shares6
import array

class taskCLController:
    #Define states
    S0_Init             = 0
    S1_WaitForCollect   = 1
    S2_OutputPosition   = 2
    
    def __init__(self, run_interval, collect_time, controller, encoder, motor, Vref):
        '''
        @brief                  Creates a taskCLController object
        @param run_interval     Time between runs in microseconds
        @param collect_time     Data collection time length in microseconds
        @param controller       Controller object used in closed loop configuration
        @param encoder          An EncoderDriver object used to collect encoder data
        @param motor            A MotorDriver object used to control motor
        @param Vref             An integer representing desired motor step response in deg/s
        '''
        #Toggle optional debug print statements
        self.debug = True        
        self.run_interval = run_interval
        self.collect_time = collect_time
        self.controller = controller
        self.encoder = encoder
        self.motor = motor
        self.Vref = Vref
        #ensure motor control is disabled
        self.motor.disable()
        self.current_time = utime.ticks_us()
        self.next_time = utime.ticks_add(self.current_time, self.run_interval)
        print('taskCLController object created using {:} controller'.format(self.controller))
        self.state = self.S0_Init
        
    def run(self):
        '''
        @brief Runs one task iteration
        '''
        self.current_time = utime.ticks_us()
        if utime.ticks_diff(self.current_time, self.next_time) > 0:
            if(self.state == self.S0_Init):
                #state 0               
                self.transitionTo(self.S1_WaitForCollect)
            elif(self.state == self.S1_WaitForCollect):
                #state 1
                if shares6.collect == True:
                    #start time is data collection start time
                    self.start_time = self.current_time
                    #end time is data collect time plus current time
                    self.end_collect_time = utime.ticks_add(self.current_time, self.collect_time)
                    self.encoder.set_position(0)
                    self.run_number = 0
                    #clear data collection arrays
                    shares6.encoder_position_array = array.array('i')
                    shares6.time_array = array.array('i')
                    shares6.encoder_velocity_array = array.array('f')
                    #set proportional controller Kp to user input
                    self.controller.set_Kp(shares6.Kp)
                    self.debugprint('Controller Kp set to {:}'.format(self.controller.get_Kp()))
                    #enable motor control
                    self.motor.enable()
                    self.transitionTo(self.S2_OutputPosition)
                    
            elif(self.state == self.S2_OutputPosition):
                #state 2
                if utime.ticks_diff(self.current_time, self.end_collect_time) > 0:
                    #data collection done
                    shares6.collect = False
                    self.motor.disable()
                    self.transitionTo(self.S1_WaitForCollect)
                else:
                    #collect data point
                    self.encoder.update()
                    self.update_position_array()
                    self.update_time_array()
                    self.debugprint('Encoder at {:} degrees'.format(str(float(self.encoder.get_position())/self.encoder.ticks_per_deg)))
                    #send encoder delta to controller
                    self.debugprint('Encoder delta {:} degrees'.format(str(float(self.encoder.get_delta()/self.encoder.ticks_per_deg))))
                    #deg/sec = (ticks/run)/(ticks/deg)/((usec/run)/(1e6 usec/sec))
                    self.Vmeas = float(self.encoder.get_delta()/self.encoder.ticks_per_deg/(self.run_interval/1e6))
                    self.update_velocity_array()                    
                    self.debugprint('Encoder velocity {:} deg/sec'.format(self.Vmeas))
                    self.debugprint('Desired velocity {:} deg/sec'.format(self.Vref))
                    self.controller.update(self.Vref, self.Vmeas)
                    self.debugprint('Controller output L = {:}'.format(self.controller.L))
                    #send L value to motor driver
                    self.motor.setDuty(-1*self.controller.L)
                
            else:
                print('Invalid state code')
                
    def transitionTo(self, next_state):
        '''
        @brief              Sets taskUI state to specified next state
        @param next_state   Variable representing the next state for the FSM
        '''
        self.state = next_state
        self.debugprint('CLController Switch to state '+ str(next_state))
        
    def update_time_array(self):
        #append time after start in microseconds to end of time array
        shares6.time_array.append(utime.ticks_diff(self.current_time, self.start_time))
        
    def update_position_array(self):
        '''
        '''
        shares6.encoder_position_array.append(round(self.encoder.get_position()/self.encoder.ticks_per_deg))
    
    def update_velocity_array(self):
        '''
        '''
        shares6.encoder_velocity_array.append(self.Vmeas)
        
    def debugprint(self, debugstring):
        '''
        @brief                  Prints string if debug parameter is true
        @details                Used for development purposes
        @param debugstring      String to print
        '''
        if self.debug == True:
            print(debugstring)