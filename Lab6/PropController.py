# -*- coding: utf-8 -*-
'''
@file       PropController.py

@brief      PropController object class
@details    Includes PropController class that defines functions for the 
            PropController object, allowing it to be controlled by the finite 
            state machine.
            
@author     Ben Sahlin
@date       November 25, 2020            
'''
class PropController:
    '''
    @brief
    @details
    '''
    
    def __init__(self, Kp, sat_min, sat_max):
        '''
        @param Kp               Integer Kp value to use in closed loop proportional controller
        @param sat_min          Minimum absolute value for float output
        @param sat_max          Maximum absolute value for float output
        '''
    
        self.Kp = Kp
        self.sat_min = sat_min
        self.sat_max = sat_max
        
        
    def update(self, Vref, Vmeas):
        '''
        @param Vref     A float representing reference velocity (desired velocity)
        @param Vmeas    A float representing measured velocity (actual velocity)
        @return         Returns the integer percentage value L
        '''
        #round L values to full integer percentages between 0 and 100
        self.L = round(self.Kp*(Vref-Vmeas))
        if self.L >= self.sat_max:
            self.L = self.sat_max
        elif self.L <= self.sat_min:
            self.L = self.sat_min
        return self.L        
    
    def get_Kp(self):
        '''
        @brief  Returns current controller Kp value
        '''
        return self.Kp
    
    def set_Kp(self, new_Kp):
        '''
        @brief          Sets controller to new Kp value
        @param new_Kp   New desired Kp value
        '''
        self.Kp = new_Kp
        