# -*- coding: utf-8 -*-
'''
@file       FSM_BackEnd.py

@brief      Task for backend management
@details    Includes finite state machine for managing user input and data output 
            through virtual USB connection for Lab 06. Designed using Nucleo L476RG 
            along with ME 305 PCB assembly. USB port on PCB assembly used for 
            PC to Nucleo communication.
            
@author     Ben Sahlin
@date       November 25, 2020
'''
import utime
import shares6

class taskBackEnd:
    #Define states
    S0_Init         = 0
    S1_WaitForCmd   = 1
    S2_WaitForData  = 2
    S3_SendData     = 3
    
    def __init__(self, run_interval, USB_VCP):
        '''
        '''
        self.debug = False
        self.run_interval = run_interval
        self.usbv = USB_VCP
        self.debugprint('taskBackEnd object created with {:} run interval and {:} uart object'.format(self.run_interval, self.usbv))
        self.current_time = utime.ticks_us()
        self.next_time = utime.ticks_add(self.current_time, self.run_interval)
        self.state = self.S0_Init
        
    def run(self):
        if(self.state == self.S0_Init):
            #state 0
            self.transitionTo(self.S1_WaitForCmd)
        elif(self.state == self.S1_WaitForCmd):
            #state 1
            if self.usbv.any() == True:
                #check for valid input- checked in frontend
                self.inputKp = self.usbv.readline().decode('ascii').strip()
                self.inputKp = float(self.inputKp)
                if self.inputKp <= 20.0 and self.inputKp >= 0.001:
                    #Kp is a valid choice
                    self.inputKp = self.inputKp/shares6.Vdc
                    self.debugprint('Setting shares Kp to {:}'.format(self.inputKp))
                    shares6.Kp = self.inputKp
                    shares6.collect = True
                    self.transitionTo(self.S2_WaitForData)
                else:
                    self.usbv.write('Invalid input to backend')
        elif(self.state == self.S2_WaitForData):
            #state 2
            #wait for shares6.collect to be set to false by CLController
            if shares6.collect == False:
                self.transitionTo(self.S3_SendData)
                
        elif(self.state == self.S3_SendData):
            #state 3
            #check array lengths to make sure they match
            if len(shares6.time_array) == len(shares6.encoder_position_array) and len(shares6.time_array) == len(shares6.encoder_velocity_array):                                 
                self.usbv.write('System collected {:} data points\n'.format(len(shares6.time_array)))
                self.usbv.write('Data sending with length\r\n')
                self.usbv.write('{:}\r\n'.format(len(shares6.time_array)))
                #send tuples of [time, position, velocity]
                for i in range(len(shares6.time_array)):
                    self.usbv.write('{:},{:},{:}\r\n'.format(shares6.time_array[i], shares6.encoder_position_array[i], shares6.encoder_velocity_array[i]))
                self.usbv.write('Data sent\r\n')
            else:
                print('System data collection error')
            self.transitionTo(self.S1_WaitForCmd)
        else:
            print('Invalid state code')
            
    def transitionTo(self, next_state):
        '''
        @brief              Sets taskUI state to specified next state
        @param next_state   Variable representing the next state for the FSM
        '''
        self.state = next_state
        self.debugprint('BackEnd Switch to state '+ str(next_state))      
        
    def debugprint(self, debugstring):
        '''
        @brief                  Prints string if debug parameter is true
        @details                Used for development purposes
        @param debugstring      String to print
        '''
        if self.debug == True:
            print(debugstring)