# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 20:04:46 2020

@author: ben
"""
#echo serial inputs

import serial

ser = serial.Serial(port = 'COM4', baudrate = 115273, timeout = 1)

while True:
    if ser.in_waiting > 0:
        readString = str(ser.readline().decode('ascii')).strip()
        print(readString)