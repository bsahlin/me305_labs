var shares6_8py =
[
    [ "collect", "shares6_8py.html#a2f05d77b98ba1330622303b34abe6464", null ],
    [ "encoder_delta", "shares6_8py.html#ae1d4b4ae81e1fd81ae0368e1ebd7ee2c", null ],
    [ "encoder_position", "shares6_8py.html#a0aabcd701b7bcc4de55367061a079f20", null ],
    [ "encoder_position_array", "shares6_8py.html#a282e549feadd5cda93579905447d49ad", null ],
    [ "encoder_velocity_array", "shares6_8py.html#a5420e97f3be4c68fed95bd182644224c", null ],
    [ "encoder_zeroing", "shares6_8py.html#a06befc4d8658dd644004ef51db5453f6", null ],
    [ "Kp", "shares6_8py.html#ac733fe10d3466300d0a48f5a31c5283e", null ],
    [ "time_array", "shares6_8py.html#a4db433010f63e7395fd6e649809600b4", null ],
    [ "Vdc", "shares6_8py.html#a12290c568fe53cc48345a7fbdb694636", null ]
];