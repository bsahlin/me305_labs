var classEncoderDriver_1_1EncoderDriver =
[
    [ "__init__", "classEncoderDriver_1_1EncoderDriver.html#a1f29b39a145e5a4ef65ab07cf0322ef8", null ],
    [ "debugprint", "classEncoderDriver_1_1EncoderDriver.html#a080aa6419170424a603e20ae5f298d8a", null ],
    [ "get_delta", "classEncoderDriver_1_1EncoderDriver.html#a939ca4bcadbaefd1bdf2ee48c07fdecd", null ],
    [ "get_position", "classEncoderDriver_1_1EncoderDriver.html#a36537be2fe38effa7a34853e5ba722ae", null ],
    [ "set_position", "classEncoderDriver_1_1EncoderDriver.html#a06fa8c8d499b314a489959aa304d19cd", null ],
    [ "update", "classEncoderDriver_1_1EncoderDriver.html#a801d099176eaeb5ae628fef95f978680", null ],
    [ "ch1_number", "classEncoderDriver_1_1EncoderDriver.html#aa667d78ada6cb0caa5dfb8e9fde53ee7", null ],
    [ "ch1_pin", "classEncoderDriver_1_1EncoderDriver.html#ac2fb63e9ed45693f208360b55e9a9801", null ],
    [ "ch2_number", "classEncoderDriver_1_1EncoderDriver.html#a617a407bfd3846b79f18d1a5741c2ee7", null ],
    [ "ch2_pin", "classEncoderDriver_1_1EncoderDriver.html#a35e835de73c06bd839f76ddf8bd8a7f0", null ],
    [ "debug", "classEncoderDriver_1_1EncoderDriver.html#ac321574d10ee3c937cfa0954c7a11a82", null ],
    [ "delta", "classEncoderDriver_1_1EncoderDriver.html#a60a87a2342a68b354f7dd83f7fe08b2b", null ],
    [ "per_multiplier", "classEncoderDriver_1_1EncoderDriver.html#ae340ff5d09fba233a21cfa691e2c22af", null ],
    [ "position", "classEncoderDriver_1_1EncoderDriver.html#a3ff85fbd31dcb3aa8d7aa588fba8c017", null ],
    [ "ticks_per_deg", "classEncoderDriver_1_1EncoderDriver.html#ac412d371f9274284fef90c0cb5988bd8", null ],
    [ "tim", "classEncoderDriver_1_1EncoderDriver.html#a757722422a8e60f631d9b1a97744992d", null ],
    [ "timer_number", "classEncoderDriver_1_1EncoderDriver.html#a4d21632f04d6f581f9de94238e2c6224", null ]
];