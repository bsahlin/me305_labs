var classPropController_1_1PropController =
[
    [ "__init__", "classPropController_1_1PropController.html#afa276b9c29fac021255ef600fd9e7305", null ],
    [ "get_Kp", "classPropController_1_1PropController.html#aea43965607e039ab690b7cfd611141c6", null ],
    [ "set_Kp", "classPropController_1_1PropController.html#a00505aee2bcdc8fd172b8c8c727f7077", null ],
    [ "update", "classPropController_1_1PropController.html#a56077b6af8ace4882bb4ac0f9eea0173", null ],
    [ "Kp", "classPropController_1_1PropController.html#a7950d70a6d3f1f0b9c7ded4966c61ea1", null ],
    [ "L", "classPropController_1_1PropController.html#a4a1e84a8a578d82f808506e300cfdc99", null ],
    [ "sat_max", "classPropController_1_1PropController.html#ae7aef145d1833909aba08f52df5b3492", null ],
    [ "sat_min", "classPropController_1_1PropController.html#af63e6a40253a3db5b22fee3d1baf488c", null ]
];