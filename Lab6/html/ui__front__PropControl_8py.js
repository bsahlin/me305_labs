var ui__front__PropControl_8py =
[
    [ "getInput", "ui__front__PropControl_8py.html#a623830057a7bfcdb08c2904d60fb589a", null ],
    [ "isfloat", "ui__front__PropControl_8py.html#a40b36e70214761fb716032a67e8061bc", null ],
    [ "plotPosition", "ui__front__PropControl_8py.html#a871bf19cf44fe8cd5fd96b95b092f994", null ],
    [ "plotVelocity", "ui__front__PropControl_8py.html#ad56b78fcf9735930672b60cc0aa968a9", null ],
    [ "readLines", "ui__front__PropControl_8py.html#a51bb2f17897a6f46273c4f0f5598878f", null ],
    [ "saveCSV", "ui__front__PropControl_8py.html#a3ea9f03f088bde4dae835f2a1fa754da", null ],
    [ "array_length", "ui__front__PropControl_8py.html#add73be0ee59f3abb10e4a555535fa1cf", null ],
    [ "collect_data_front", "ui__front__PropControl_8py.html#acaca9364236d016a40a9732c423999a0", null ],
    [ "collect_interrupt", "ui__front__PropControl_8py.html#acac841e4e90bee0fc19399100b0e9436", null ],
    [ "Kp", "ui__front__PropControl_8py.html#a70cb2eac936103be1da12e9acc6b54ac", null ],
    [ "position_array", "ui__front__PropControl_8py.html#acc33ea57040f3b186e058b44dd6b8d76", null ],
    [ "readString", "ui__front__PropControl_8py.html#aff8f118058494e89f55dc48ebd8a6980", null ],
    [ "ser", "ui__front__PropControl_8py.html#a46df2da6f8aead3f3fd83d970072a39b", null ],
    [ "time_array", "ui__front__PropControl_8py.html#a0bab6a2f46e6c4b9c2f821c95a638160", null ],
    [ "userInput", "ui__front__PropControl_8py.html#a28fc7e1ae038e61b438629876287f0de", null ],
    [ "velocity_array", "ui__front__PropControl_8py.html#afde4c5cc3914d1b57e9bf40f989ad051", null ]
];