# -*- coding: utf-8 -*-
'''
@file       ui_front_PropControl.py

@brief      Frontend UI file for Lab 06
@details    Communicates through serial port to Nucleo board. Prompts for user 
            inputted proportional controller gain Kp and starts data collection.

@author     Ben Sahlin
@date       November 25, 2020
'''
import serial
import numpy as np
import matplotlib.pyplot as plt

def getInput():
    '''
    @brief      Gets user input from PC command line
    '''
    userInput = input('Spyder: Enter a Kp value between 0.001 and 20 to start data collection or press E to exit\n')
    return userInput

def readLines():
    '''
    @brief      Reads and prints next line from serial port
    '''
    readString = str(ser.readline())
    print(str(readString))    

def plotPosition(time_array, position_array):
    '''
    @brief                  Plots time and postion encoder data
    @details                Uses PyPlot from MatPlotLib to plot data sent from Nucleo board     
    @param time_array       Time array in microseconds
    @param position_array   Position array in encoder pulses
    '''
    #adjust time array to seconds from microseconds
    time_array = time_array/10**6
    plt.plot(time_array, position_array)
    plt.xlim([0,2])
    plt.xlabel('Time (sec)')
    plt.ylabel('Position (degrees)')
    plt.title('Output Shaft Position vs Time')
    plt.show()
    print('\rSpyder: Position data plotted')
    
def plotVelocity(time_array, velocity_array, Kp):
    '''
    @brief                  Plots time and velocity encoder data
    @details                Uses PyPlot from MatPlotLib to plot data sent from Nucleo board     
    @param time_array       Time array in microseconds
    @param velocity)array   Velocity array in degrees per second
    @param Kp               Proportional gain used to generate plot
    '''
    #adjust time array to seconds from microseconds
    time_array = time_array/10**6
    plt.plot(time_array, velocity_array)
    plt.xlim([0,2])
    plt.xlabel('Time (sec)')
    plt.ylabel('Velocity (degrees/sec)')
    plt.title('Output Shaft Velocity vs Time for Kp = {:}'.format(Kp))
    plt.show()
    print('\rSpyder: Velocity data plotted')    
    
def saveCSV(time_array, position_array, filename):
    '''
    @brief                  Saves data as CSV file
    @details                Uses Numpy to save encoder data in a CSV file with 
                            a specified filename   
    @param time_array       Time array in seconds
    @param position_array   Encoder position array in degrees
    '''
    np.savetxt(filename, (time_array, position_array), delimiter = ',')
    print('Spyder: Data saved to '+filename)
    
def isfloat(value):
    '''
    @brief          Checks if input can be cast to a float
    @details        Attempts to cast inputted value to a float, returns True if 
                    it can be cast and False if it can not.
    @param value    variable to attempt to cast to float
    '''
    try:
        float(value)
        return True
    except ValueError:
        return False

while True:
    collect_data_front = False
    userInput = getInput()
    ser = serial.Serial(port = 'COM4', baudrate = 115273, timeout = 1)
    if userInput == 'E':
        print('Spyder: Exiting')
        ser.close()
        break
    #check if input is valid
    elif(isfloat(userInput) == True):
        if float(userInput) >= 0.001 and float(userInput) <= 20:
            Kp = userInput
            ser.write(userInput.encode('ascii'))
            collect_data_front = True
            collect_interrupt = False
        else:
            print('Spyder: Input is not between 0.1 and 5')
            
    elif(isfloat(userInput) == False):
        print('Spyder: Input is not a valid Kp')
    else:
        print('Spyder: Invalid input')
            
    while collect_data_front == True:
        #print(ser.in_waiting)
        if ser.in_waiting > 0:
            #if anything waiting in serial
            readString = str(ser.readline().decode('ascii')).strip()
            #print('Spyder: readstring is '+readString)        
            if readString == 'Data sending with length':
                #if reading says it's sending data
                print('Spyder: Reading data')
                array_length = int(ser.readline())
                #create arrays
                time_array = np.empty(array_length)
                position_array = np.empty(array_length)
                velocity_array = np.empty(array_length)
                print('Spyder: Array length of '+str(array_length))
                for i in range(array_length):
                    [time_array[i], position_array[i], velocity_array[i]] = ser.readline().decode('ascii').strip().split(',')
                    print('Spyder: Time is: {:}, Position is: {:}, Velocity is {:}'.format(time_array[i], position_array[i], velocity_array[i]))
                #if next readstring is not 'Data sent', there is an error
                readString = str(ser.readline().decode('ascii')).strip()            
                if readString == 'Data sent':
                    print('Nucleo: '+readString)
                    print('Spyder: Data recieved')
                    plotVelocity(time_array, velocity_array, Kp)                    
                    collect_data_front = False   
                else:
                    print('Spyder: Data Read Error, readstring is: '+readString)
            #elif just print reading
            else:
                print('Nucleo: '+readString)
    ser.close()