# -*- coding: utf-8 -*-
'''
@file       encoder.py
@brief      Encoder object class
@details    Includes encoder class that defines functions for the encoder 
            object, allowing it to be controlled by the finite state machine.

@author     Ben Sahlin
@date       October 21, 2020
'''

import pyb
import shares

class encoder:
    
    def __init__ (self, timer_number, ch1_pin_name, ch2_pin_name, ch1_number, ch2_number):
        '''
        @brief                  Creates an encoder object
        @details                Creates encoder object with specified 
                                parameters, sets initial position and initial 
                                shared variable values.
        @param timer_number     Variable representing timer number to use
        @param ch1_pin_name     Variable representing Channel 1 Pin to use
        @param ch2_pin_name     Variable representing Channel 2 Pin to use
        @param ch1_number       Variable representing Channel 1 Number to use
        @param ch2_number       Variable representing Channel 2 Number to use
        '''
        #set up inputted parameters
        self.timer_number = timer_number
        self.ch1_number = ch1_number
        self.ch2_number = ch2_number        
        #create pin objects with inputted pins
        self.ch1_pin = pyb.Pin(ch1_pin_name)
        print('Pin 1 created')
        self.ch2_pin = pyb.Pin(ch2_pin_name)
        print('Pin 2 created')

        #create timer object and channels corresponding to pin objects
        self.tim = pyb.Timer(self.timer_number)
        self.tim.init(prescaler=0, period=0xFFFF)
        self.tim.channel(ch1_number, pin = self.ch1_pin, mode=pyb.Timer.ENC_AB)
        self.tim.channel(ch2_number, pin = self.ch2_pin, mode = pyb.Timer.ENC_AB)
        
        #set up initial position at 0
        self.set_position(0)
        #set share file values
        shares.encoder_position = self.get_position()
        shares.encoder_delta = self.delta
                
    def update(self):
        '''
        @brief      Updates encoder position
        @details    Finds new encoder position based on current measured 
                    position, taking into account timer overflow. Also checks 
                    to see if encoder should be reset, and updates shares 
                    variables.
        '''
        if (shares.encoder_zeroing == False):       
            #move current position to old position
            self.position[0] = self.position[1]
            #upate current position, could be bad position
            self.position[1] = self.tim.counter()
            #print(str(self.position) +' after swap to current to old')
            self.get_delta()
            if (self.delta > 0.5*self.tim.period()):
                #bad delta value
                if (self.delta<0):
                    #going forwards/CW
                    self.delta += self.tim.period()
                    #print('after addition delta = '+str(self.delta))
                elif (self.delta>0):
                    #going backwards/CCW
                    self.delta -= self.tim.period()
                    #print('after subraction delta = '+str(self.delta))
                else:
                    print('Delta calc error')   
            #update to real current position based on delta
            self.position[1] = self.position[0] + self.delta
            #print(str(self.position[1]))
        elif (shares.encoder_zeroing == True):
            self.set_position(0)
            shares.encoder_zeroing = False
        else:
            print('Error with shares.encoder_zeroing')
            
        #set shares encoder position and delta
        shares.encoder_position = self.get_position()
        shares.encoder_delta = self.get_delta()
        #print(str(shares.encoder_position))

    
    def get_delta(self):
        '''
        @brief      Returns and sets current encoder delta
        @details    Calculates delta based on previous and current encoder 
                    positions.
        '''
        #delta = new position - old position
        self.delta = self.position[1]-self.position[0]
        #print('delta subraction results in '+str(self.delta))             
        return self.delta
    
    def get_position(self):
        '''
        @brief      Returns current encoder position
        @details    Returns current position without running update function
        '''
        return self.position[1]
    
    def set_position(self, desired_position):
        '''
        @brief                      Sets encoder position to desired position
        @details                    Sets encoder previous and current positions to inputted 
                                    position and recalculates delta.
        @param desired_position     Variable representing a position to set encoder to.
        '''
        self.tim.counter(desired_position)
        self.position = [desired_position, desired_position]
        self.delta = self.get_delta()