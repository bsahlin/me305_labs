# -*- coding: utf-8 -*-
'''
@file lab3main.py
@brief Main file for Lab 03
@details Creates task objects and runs tasks for encoder and UI.
@author Ben Sahlin
@date October 21, 2020
'''

import encoder
from FSM_encoder import taskEncoder
import pyb
from machine import UART
from FSM_UI import taskUI
import shares

enc1 = encoder.encoder(3,'A6','A7',1,2)

interval = 1*10**-3

enctask1 = taskEncoder(interval, enc1)
uitask1 = taskUI(interval)

while True:
    enctask1.run()
    uitask1.run()