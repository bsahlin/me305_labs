'''
@file       FSM_UI.py
@brief      Task for UI control in Lab 03
@details    Includes finite state machine for running UI task. Displays 
            available commands to control the encoder and checks for 
            user input. Can print current encoder position, print current 
            encoder delta, and set encoder position to zero.
            
@author     Ben Sahlin
@date       October 21, 2020
'''

import utime
from machine import UART
import shares

class taskUI:
    ## Constant defining State 0 - Initialization
    S0_Init = 0
    ## Constant defining State 1 - Waiting for Char
    S1_Wait_For_Char = 1
    ## Constant defining State 2 - Execute Command
    S2_Exectute_Command = 2
    #create uart object to take in user inputs
    uart = UART(2)    
    
    def __init__(self, run_interval):
        '''
        @brief                  Creates a taskUI object
        @param run_interval     Time between runs in milliseconds
        @details                Initializes UI task with start time, run 
                                interval, and state.
        '''
        print('taskUI created')
        self.run_interval = run_interval
        self.state = self.S0_Init
        self.start_time = utime.ticks_us()
        self.next_time = utime.ticks_add(self.start_time, self.run_interval)
        
    def run(self):
        '''
        @brief      runs one iteration of the task
        '''
        self.current_time = utime.ticks_us()
        if (utime.ticks_diff(self.current_time, self.next_time)):
            if (self.state == self.S0_Init):
                #state 0
                self.transitionTo(self.S1_Wait_For_Char)
                #self.printCommands()
            elif(self.state == self.S1_Wait_For_Char):
                #state 1
                if (self.uart.any() >=1 ):
                    self.transitionTo(self.S2_Exectute_Command)
            elif(self.state == self.S2_Exectute_Command):
                #run state 2 code
                self.input = self.uart.read()                
                if (self.input == bytes('p', 'ascii')):
                    print('Posiiton of encoder is: ' + str(shares.encoder_position)+'\n')
                elif (self.input == bytes('d', 'ascii')):
                    print('Delta of encoder is: ' + str(shares.encoder_delta)+'\n')
                elif(self.input == bytes('z', 'ascii')):
                    shares.encoder_zeroing = True
                    print('Encoder zeroed\n')
                elif(self.input == bytes('G')):
                    print('Starting data collection for 10 seconds, press S to stop')
                    shares.collect = True
                elif(self.input == bytes('S')):
                    #stop collecting
                    pass
                else:
                    print('Invalid command\n')
                    self.printCommands()
                self.transitionTo(self.S1_Wait_For_Char)
            else:
                #invalid state code
                print('Invalid state')
            self.next_time = utime.ticks_add(self.current_time, self.run_interval)
            
    def transitionTo(self, next_state):
        '''
        @brief              Sets taskUI state to specified next state
        @param next_state   Variable representing the next state for the FSM
        '''
        self.state = next_state
    
    def printCommands(self):
        '''
        @brief      Prints available encoder commands
        '''
        print('''Available Commands:
              \n'z' Zero the encoder position
              \n'p' Print the encoder position
              \n'd' Print the encoder delta\n''')