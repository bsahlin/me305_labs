# -*- coding: utf-8 -*-
'''
@file mainpage_lab3.py

@page page_Lab3 Lab 03
    @section sec_Lab3_intro Introduction
        Lab 03 introduces the use of an encoder. The goal of the encoder is to 
        measure how far a motor spins. In this case, we just spun the motor by 
        hand, not by controlling it with the Nucleo board. The goal of this 
        lab was to use multiple state machies to allow for multitasking 
        encoder position updating while accepting user inputs. Specifically, 
        the user is able to print the encoder position, print the encoder 
        delta, or zero the encoder position.
        
    @section sec_Lab03_FSMdiagram FSM Diagrams
        The finite state machine diagram for the encoder looks like this: 
        @image html encoder_fsmdiagram.png
        
        The finite state machine diagram for the user interface looks like this:
        @image html ui_fsmdiagram.png

    @section sec_Lab03_source Source Code
        The source code for the lab assignment can be found in this BitBucket 
        repository: 
        [<b>Lab 03 Repository</b>](https://bitbucket.org/bsahlin/me305_labs/src/master/Lab3/lab3main.py)</b>

    @section sec_Lab02_files Files
        @ref lab3main.py \n
        @ref FSM_encoder.py \n
        @ref FSM_UI.py \n
        @ref encoder.py \n
        @ref shares.py \n

'''