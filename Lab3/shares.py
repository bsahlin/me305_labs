# -*- coding: utf-8 -*-
"""
Created on Wed Oct 21 21:38:30 2020

@file shares.py

@brief      A file containing shared variables for Lab 03
@details    Contains shared variables for encoder position, encoder delta, and 
            whether or not the encoder should be reset to zero.

@author     Ben Sahlin
@date       October 21, 2020
"""

encoder_position = None
encoder_delta = None
encoder_zeroing = False