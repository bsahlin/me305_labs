# -*- coding: utf-8 -*-
'''
@file       FSM_LED.py

@brief      Class and functions for LED control in Lab 02.
@details    Includes LED task which allows for control of real and virtual 
            LED's. For real LED, sets brightness as a ramp function that cycles
            at the specified blink interval. For virtual LED, cycles ON and OFF 
            at the specified blink interval. Uses Micropython libraries and was 
            designed with Nucleo L476RG board.
            
@author     Ben Sahlin

@date       October 14, 2020
                        
'''

import utime
import pyb

class taskLED:
    ## Constant defining State 0 - Initialization
    S0_Init         = 0
    ## Constant defining State 1 - Output Value
    S1_OutputVal    = 1

    
    def __init__(self, blink_interval, led_type):
        '''
        @brief                  Creates a taskLED object
        @param blink_interval   A variable representing the interval between blinks
        @param run_interval     A variable representing the interval between runs
        @param led_type         A variable representing the LED type, 0 for real and 1 for virtual
        @param max_val          A variable representing the max value of LED brightness in percentage
        @param inc              A variable representing the increment in LED brightness per task iteration
        @param start_time       A variable representing task start time
        @param next_time        A variable representing the time to start the next task iteration
        @param pinA5            A pin object corresponding to the desired pin for a real LED
        @param tim              A timer object used to control PWM for a real LED 
        @param t1ch1            A timer channel object used to set PWM for a real LED
        @param toggle           A boolean representing ON or OFF for a virtual LED
        '''
        print('led task created')
        ## The state to run on the next iteration of the task
        self.state = self.S0_Init
        ## Counter for number of times task has run
        self.runs = 0
        ## Convert blink interval inputted to microseconds
        self.blink_interval = int(blink_interval*1e6)
        print('period of '+ self.blink_interval +' microseconds')
        ## Time in seconds between task runs, task runs 100 times/inputted interval
        self.run_interval = int(self.blink_interval/100)
        ## Physical LED = 0, Virtual LED = 1
        self.led_type = led_type
        ## Set max value of LED
        self.max_val = 100
        ## Set increment based on both intervals and max value
        self.inc = self.max_val/self.blink_interval*self.run_interval
        ## Timestamp for first iteration
        self.start_time = utime.ticks_us()
        ## Timestamp for when task should run next
        self.next_time = utime.ticks_add(self.start_time, self.run_interval)
        
        if (self.led_type == 0):
            ## Pin for real LED
            self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
            ## Create timer
            self.tim = pyb.Timer(2, freq=20000)
            ## Create timer channel
            self.t1ch1 = self.tim.channel(1, pyb.Timer.PWM, pin=self.pinA5)
            ## Set real LED to 0 brightness
            self.t1ch1.pulse_width_percent(0)
        elif(self.led_type == 1):
            ## Set toggle for virtual LED
            self.toggle = True
        
    def run(self):
        '''
        @brief      Runs one task iteration
        @param val  A variable representing the value of physical LED brightness
        '''        
        self.current_time = utime.ticks_us()
        if utime.ticks_diff(self.current_time, self.next_time) > 0: #difference between current and next times is greater than 0
            if(self.state == self.S0_Init):
                #run State 0 code
                #set intial value of LED brightness to 0
                self.val = 0
                self.transitionTo(self.S1_OutputVal)
            elif(self.state == self.S1_OutputVal):
                #run State 1 code
                self.incVal()
                if(self.led_type == 0):
                    # LED is physical
                    #self.incVal()
                    self.t1ch1.pulse_width_percent(self.val)
                elif(self.led_type == 1):
                    # LED is virtual
                    currenttoggle = self.toggle
                    #self.incVal()
                    if (currenttoggle != self.toggle):
                        #print(self.toggle)
                        pass
                else:
                    #invalid LED type value
                    print('Invalid LED type')                    
            else:
                #invalid state
                print('Invalid state code')
                
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.run_interval)
            
    def transitionTo(self, newState):
        '''
        @brief              Sets the state variable to the specified new state
        @param newState     The next state to run
        '''
        self.state = newState
        
    def incVal(self):
        '''
        @brief      Increments value of LED brightness
        '''
        if (self.val >= self.max_val):
            self.val = 0
            if(self.led_type == 1):
                self.toggle = not(self.toggle) #toggles on/off LED tracker
                print(self.toggle)
        else:
            self.val += self.inc        
        
        