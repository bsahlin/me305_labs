# -*- coding: utf-8 -*-
'''
@file mainpage_lab2.py

@page page_Lab2 Lab 02
    @section sec_Lab2_intro Introduction
        HW 00 looks at creating finite state machines (FSM). For this 
        assignment the system was an elevator with two floors. There are four 
        buttons total, two inside to select the desired floor and two that are 
        triggered when the elevator is on Floor 1 and Floor 2 respectively. 
        There is also a motor which can spin forwards, backwards, and stop.
        
        Lab 02 introduces the Nucleo board and the micropython library. The 
        goal of the lab was to create a finite state machine (FSM) that could 
        be used to control the operation of a virtual and physical LED.
        
    @section sec_Lab02_FSMdiagram FSM Diagram
        The finite state machine diagram for each LED looks like this: 
        @image html led_fsmdiagram.png

    @section sec_Lab02_pattern LED Patterns
        The physical led brightness follows this pattern:
        @image html led_pattern.png width=800px
        The virtual led turns on for one period of the shown cycle, then turns 
        off for the next period, and repeats this pattern.

    @section sec_Lab02_source Source Code
        The source code for the homework assignment can be found in this BitBucket 
        repository: 
        [<b>Lab 02 Repository</b>](https://bitbucket.org/bsahlin/me305_labs/src/master/Lab2/main_lab2.py)</b>

    @section sec_Lab02_files Files
        @ref main_lab2.py \n
        @ref FSM_LED.py \n

'''
