# -*- coding: utf-8 -*-
'''
@file main_lab2.py
@brief Main file for Lab 02
@details Creates task objects and runs tasks for real and virtual LED's
@author Ben Sahlin
@date October 14, 2020
'''
from FSM_LED import taskLED

LED_virt = taskLED(10, 1)
LED_real = taskLED(10, 0)

while(True):
    LED_virt.run()
    LED_real.run()