# -*- coding: utf-8 -*-
'''
@file       FSM_datagen.py

@brief      Task for encoder control in Lab 04
@details    Includes finite state machine for running encoder task. Updates 
            encoder position at specified run interval and for the inputted 
            encoder object. Uses Micropython libraries and was 
            designed with Nucleo L476RG board.

@author     Ben Sahlin
@date       October 21, 2020
'''

import utime
import shares
import encoder
import array
from machine import UART

class taskDataGen:
    ## Constant defining State 0 - Initialization
    S0_Init             = 0
    ## Constant defining State 1 - Output Position
    S1_OutputPosition   = 1
    ## Constant defnining State 2 - Send Vals
    S2_SendVals         = 2
    
    def __init__ (self, run_interval, run_time, encoder):
        '''
        @brief                  Creates a taskEncoder object
        @param run_interval     Time between runs in milliseconds
        @param run_time         Time before stopping data collection without interrupt in milliseconds
        @param encoder          Encoder object to use in task
        '''
        print('Encoder task created')
        ## The encoder object to use for the task
        self.encoder = encoder
        ## The interval between runs of the task in microseconds
        self.run_interval = run_interval
        ## The state to run on the next iteration of the task
        self.state = self.S0_Init
        ## Timestamp for first iteration
        self.start_time = utime.ticks_us()
        ## Timestamp for when task should run next
        self.next_time = utime.ticks_add(self.start_time, self.run_interval)
        ## Time task should repeat for
        self.run_time = run_time
        ## Timestamp for when task should stop repeating (updated in S0)
        #self.end_time = self.start_time
        ## Run count of runs through encoder update
        self.run_number = 0
        
        
    def run(self):
        '''
        @brief  Runs one task iteration
        '''
        self.current_time = utime.ticks_us()
        
        if utime.ticks_diff(self.current_time, self.next_time) > 0:
            if (self.state == self.S0_Init):
                if (shares.collect == True):
                    #reset values
                    self.start_time = self.current_time
                    self.end_time = utime.ticks_add(self.current_time, self.run_time)
                    self.encoder.set_position(0)
                    self.run_number = 0
                    shares.encoder_position_array = array.array('i')
                    shares.time_array = array.array('i')
                    self.transitionTo(self.S1_OutputPosition)
            elif (self.state ==  self.S1_OutputPosition):
                #run State 1 code
                self.encoder.update()
                self.encoder.update_position_array()
                self.update_time_array()
                #print(shares.encoder_position_array)
                self.run_number += 1
                if(shares.collect == False or utime.ticks_diff(self.current_time, self.end_time) > 0):
                    shares.collect = False
                    shares.data_sent = False
                    self.transitionTo(self.S2_SendVals)
            elif (self.state == self.S2_SendVals):
                #print('sending vals')
                if (shares.collect == True):
                    #reset values
                    self.start_time = self.current_time
                    self.end_time = utime.ticks_add(self.current_time, self.run_time)
                    self.encoder.set_position(0)
                    self.run_number = 0
                    shares.encoder_position_array = array.array('i')
                    shares.time_array = array.array('i')
                    self.transitionTo(self.S1_OutputPosition)
                elif(shares.data_sent == False):
                    #add uart or print statement to send to PC with placeholders and timestamps
                    print('Position array collected {:} data points'.format(len(shares.encoder_position_array)))
                    print('Time array collected {:} data points'.format(len(shares.time_array)))
                    shares.uart.write('Data Sending with length\r\n')
                    shares.uart.write('{:}\r\n'.format(len(shares.encoder_position_array)))
                    for i in range(len(shares.encoder_position_array)):
                        shares.uart.write('{:},{:}\r\n'.format(shares.time_array[i], shares.encoder_position_array[i]))
                    #print(str(shares.time_array))
                    print('Data sent')
                    shares.data_sent = True
            else:
                print('Invalid state code')            
            self.next_time = utime.ticks_add(self.current_time, self.run_interval)
            
    def transitionTo(self,nextState):
        '''
        @brief              Sets taskUI state to specified next state
        @param next_state   Variable representing the next state for the FSM
        '''
        self.state = nextState
    
    def update_time_array(self):
        #append time after start in microseconds to end of time array
        shares.time_array.append(utime.ticks_diff(self.current_time, self.start_time))