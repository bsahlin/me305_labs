# -*- coding: utf-8 -*-
"""

@file shares.py

@brief      A file containing shared variables for Lab 04
@details    Contains shared variables for encoder position, encoder delta,
            whether or not the encoder should be reset to zero, and whether 
            task should be interrupted. Still includes all shared variables 
            from lab 03 as well.

@author     Ben Sahlin
@date       October 21, 2020
"""

import array
from machine import UART

encoder_position = None
encoder_delta = None
encoder_zeroing = False
#should data be being collected
collect = False
#has data been sent to PC
data_sent = False

#create encoder position array of integers
encoder_position_array = array.array('i')
#create time array of floats
time_array = array.array('f')

#reference arrays
condensed_tRefArray = array.array('f')
condensed_vRefArray = array.array('f')

#create uart object to take in user inputs
uart = UART(2)  