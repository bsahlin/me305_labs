# -*- coding: utf-8 -*-
'''
@file       FSM_encoder.py

@brief      Task for encoder control in Lab 03
@details    Includes finite state machine for running encoder task. Updates 
            encoder position at specified run interval and for the inputted 
            encoder object. Uses Micropython libraries and was 
            designed with Nucleo L476RG board.

@author     Ben Sahlin
@date       October 21, 2020
'''

import utime
import shares
import encoder

class taskDataGen:
    ## Constant defining State 0 - Initialization
    S0_Init             = 0
    ## Constant defining State 1 - Output Position
    S1_OutputPosition   = 1
    
    def __init__ (self, run_interval, runs_total, encoder):
        '''
        @brief                  Creates a taskEncoder object
        @param run_interval     Time between runs in milliseconds
        @param encoder          Encoder object to use in task
        '''
        print('Encoder task created')
        ## The encoder object to use for the task
        self.encoder = encoder
        ## The interval between runs of the task in miliseconds
        self.run_interval = run_interval
        ## The state to run on the next iteration of the task
        self.state = self.S0_Init
        ## Timestamp for first iteration
        self.start_time = utime.ticks_us()
        ## Timestamp for when task should run next
        self.next_time = utime.ticks_add(self.start_time, self.run_interval)
        ## Run count
        self.run_count = 0
        ## T
        
        
    def run(self):
        '''
        @brief  Runs one task iteration
        '''
        self.current_time = utime.ticks_us()
        if (utime.ticks_diff(self.current_time, self.next_time) > 0):
            if (self.state == self.S0_Init):
                #run State 0 code
                #set encoder start point to 0
                self.encoder.set_position(0)
                self.transitionTo(self.S1_OutputPosition)
            elif (self.state ==  self.S1_OutputPosition):
                #run State 1 code
                self.encoder.update()

            else:
                print('Invalid state code')
            
            self.next_time = utime.ticks_add(self.current_time, self.run_interval)            
            
            
    def transitionTo(self,nextState):
        '''
        @brief              Sets taskUI state to specified next state
        @param next_state   Variable representing the next state for the FSM
        '''
        self.state = nextState