# -*- coding: utf-8 -*-
"""
Created on Thu Oct 22 13:53:01 2020

@author: ben
"""
#plotting example

import matplotlib.pyplot as plt
import numpy as np

time = np.array([0,1,2,3,4,5,6,7,8,9,10])
vals = time**2

plt.plot(time,vals)

plt.show()

#save as csv
#np.savetxt('vals.csv', vals, delimiter = ',')
np.savetxt('./data/vals.csv', (time,vals), delimiter = ',')