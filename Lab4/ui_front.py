# -*- coding: utf-8 -*-
'''
@file       ui_front.py

@brief      Frontend UI file for Lab 04
@details    Communicates through serial port to Nucleo board. Allows starting 
            and stopping encoder data collection through serial port.

@author     Ben Sahlin
@date       November 4, 2020
'''
import serial
import numpy as np
import matplotlib.pyplot as plt
import keyboard

def getInput():
    '''
    @brief      Gets user input from PC command line
    '''
    userInput = input('Spyder: Press G to begin data collection for 10 seconds or press E to exit\n')
    return userInput

def readLines():
    '''
    @brief      Reads and prints next line from serial port
    '''
    readString = str(ser.readline())
    print(str(readString))    

def plotPosition(time_array, position_array):
    '''
    @brief                  Plots time and postion encoder data
    @details                Uses PyPlot from MatPlotLib to plot data sent from Nucleo board     
    @param time_array       Time array in microseconds
    @param position_array   Position array in encoder pulses
    '''
    #adjust time array to seconds from microseconds
    time_array = time_array/10**6
    #adjust position array pulses to degrees, enc pulses * 360 enc deg/(2**16-1) enc pulses * 50 gear ratio 
    position_array = position_array*360/(2**16-1)*50
    plt.plot(time_array, position_array)
    plt.xlim([0,10])
    plt.xlabel('Time (sec)')
    plt.ylabel('Position (degrees)')
    plt.title('Motor Position vs Time')
    plt.show()
    print('\rSpyder: Data plotted')
    
def saveCSV(time_array, position_array, filename):
    '''
    @brief                  Saves data as CSV file
    @details                Uses Numpy to save encoder data in a CSV file with 
                            a specified filename   
    @param time_array       Time array in seconds
    @param position_array   Encoder position array in degrees
    '''
    np.savetxt(filename, (time_array, position_array), delimiter = ',')
    print('Spyder: Data saved to '+filename)

while True:
    collect_data_front = False
    userInput = getInput()
    ser = serial.Serial(port = 'COM3', baudrate = 115273, timeout = 1)
    if userInput == 'G':
        ser.write(userInput.encode('ascii'))
        collect_data_front = True
        collect_interrupt = False
    elif userInput == 'E':
        print('Spyder: Exiting')
        ser.close()
        break
    elif userInput != 'G' and userInput != 'E':
        print('Spyder: Invalid input')
    while collect_data_front == True:
        if (collect_data_front == True and keyboard.is_pressed('shift+s') == True and collect_interrupt == False):
            collect_interrupt = True
            userInput = 'S'
            print(userInput)
            ser.write(userInput.encode('ascii'))
        #print(ser.in_waiting)
        if ser.in_waiting > 0:
            #if anything waiting in serial
            readString = str(ser.readline().decode('ascii')).strip()
            #print('Spyder: readstring is '+readString)        
            if readString == 'Data Sending with length':
                #if reading says it's sending data
                print('Spyder: Reading data')
                array_length = int(ser.readline())
                #create arrays
                time_array = np.empty(array_length)
                position_array = np.empty(array_length)
                print('Spyder: Array length of '+str(array_length))
                for i in range(array_length):
                    [time_array[i], position_array[i]] = ser.readline().decode('ascii').strip().split(',')
                    print('Spyder: Time is: {:}, Position is: {:}'.format(time_array[i], position_array[i]))
                #if next readstring is not 'Data sent', there is an error
                readString = str(ser.readline().decode('ascii')).strip()            
                if readString == 'Data sent':
                    print('Nucleo: '+readString)
                    print('Spyder: Data recieved')
                    plotPosition(time_array, position_array)
                    saveCSV(time_array, position_array,'./data/MotorPositionData.csv')
                    collect_data_front = False
    
                else:
                    print('Spyder: Data Read Error, readstring is: '+readString)
            #elif just print reading
            else:
                print('Nucleo: '+readString)
    ser.close()