# -*- coding: utf-8 -*-
'''
@file           main_lab4.py
@brief          Main file running on Nucleo board for Lab 04
@details        Creates and manages objects and tasks for Lab 04. To 
                automatically run on Nucleo startup, rename as main.py.

@author         Ben Sahlin
@date           November 4, 2020
'''

#from pyb import UART
import FSM_datagen
import FSM_UI
from encoder import encoder
import shares
import array

#Reset shared variables
shares.encoder_position = None
shares.encoder_delta = None
shares.encoder_zeroing = False
#should data be being collected
shares.collect = False
#has data been sent to PC
shares.data_sent = False

#create encoder position array of integers
shares.encoder_position_array = array.array('i')
#create time array of floats
shares.time_array = array.array('f')

#time between runs in microseconds
run_interval = int(1/5*10**6)
#data collection time in microseconds
run_time = int(10*10**6)
#create objects
encoder1 = encoder(3, 'A6', 'A7', 1, 2)
taskDataGen1 = FSM_datagen.taskDataGen(run_interval, run_time, encoder1)
taskUI1 = FSM_UI.taskUI(run_interval)
print('encoder object, data gen and UI tasks created')

#run FSMs
while True:
    taskDataGen1.run()
    taskUI1.run()
