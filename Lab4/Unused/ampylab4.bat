@ECHO OFF

ECHO Copying files to nucleo
ampy -p com3 put encoder.py encoder.py
ampy -p com3 put FSM_datagen.py FSM_datagen.py
ampy -p com3 put FSM_UI.py FSM_UI.py
ampy -p com3 put main.py main.py
ampy -p com3 put shares.py shares.py
ECHO Done copying