# -*- coding: utf-8 -*-
"""
Created on Thu Oct 29 16:10:32 2020

@author: ben
"""

import FSM_FrontUI


#define run interval in seconds
run_interval = .1
#create task
taskFrontUI1 = FSM_FrontUI.taskFrontUI(run_interval)

while True:
    taskFrontUI1.run()