# -*- coding: utf-8 -*-
"""
Created on Thu Oct 29 14:01:15 2020

@author: ben
"""

import time
import calendar
import serial
import numpy as np
import matplotlib.pyplot as plt
import keyboard

class taskFrontUI:
    ## Constant defining State 0 - Initialization
    S0_Init = 0
    ## Constant defining State 1 - Check for Input
    S1_Check = 1
    ## Constant defining State 2 - Send Command
    S2_Send_Command = 2
    ## Constant defining State 3 - Recieve Data
    
    def __init__(self,run_interval):
        '''
        @brief                  Creates a taskFrontUI object
        @param run_interval     Time between runs in seconds
        @details                Initializes UI task for use in front end
        '''
        self.run_interval = run_interval
        self.state = self.S0_Init
        #start time in fractional seconds
        self.start_time = time.monotonic()
        self.next_time = self.start_time + self.run_interval        
        self.ser = serial.Serial(port = 'COM3', baudrate = 115273, timeout = 1)
        print('Spyder: FrontUI task created')
        
    def run(self):
        '''
        @brief  Runs one iteration of the task
        '''
        self.current_time = time.monotonic()
        if (self.current_time > self.next_time):
            if(self.state == self.S0_Init):
                #initialize FSM
                self.printCommands()
                self.transitionTo(self.S1_Check)
            elif(self.state == self.S1_Check):
                self.ser.close()
                #state 1 code
                pass
            elif(self.state == self.S2_Send_Command):
                #state 2 code
                pass
            else:
                print('Spyder: Invalid state code')
            self.next_time = self.current_time + self.run_interval
            
    def transitionTo(self, next_state):
        '''
        @brief              Sets taskUI state to specified next state
        @param next_state   Variable representing the next state for the FSM
        '''
        self.state = next_state
        
    def printCommands(self):
        '''
        @brief      Prints available commands
        '''
        print('''Available Commands:
              \r'G' Start data collection
              \r'S' Stop data collection
              \r'E' Exit UI
              ''')
    def getInput():
        userInput = input('Press G to begin data collection for 10 seconds\n')
        return userInput
    
    def readLines(self):
        readString = str(self.ser.readline())
        print(str(readString))    
    
    def plotPosition(self, time_array, position_array):
        #plt.clear()
        #adjust time array to seconds from microseconds
        self.time_array = time_array/10**6
        #adjust position array pulses to degrees, enc pulses * 360 enc deg/(2**16-1) enc pulses * 50 gear ratio 
        self.position_array = position_array*360/(2**16-1)*50
        plt.plot(self.time_array, self.position_array)
        plt.xlim([0,10])
        plt.xlabel('Time (sec)')
        plt.ylabel('Position (degrees)')
        plt.title('Motor Position vs Time')
        plt.show()
        print('Spyder: Data plotted')
        
    def saveCSV(time_array, position_array, filename):
        np.savetxt(filename, (time_array, position_array), delimiter = ',')
        print('Spyder: Data saved to '+filename)
