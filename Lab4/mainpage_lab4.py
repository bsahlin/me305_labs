# -*- coding: utf-8 -*-
'''
@file mainpage_lab4.py

@page page_Lab4 Lab 04
    @section sec_Lab4_intro Introduction
        Lab 4 builds on the concepts in Lab 3 with encoder data recording. In 
        this lab, a tool was created to collect, plot, and save data from the 
        encoder connected to the Nucleo. This included creating a basic 
        frontend UI to run on the PC and connect to the Nucleo and control it 
        through the serial port.
    @section sec_Lab04_FSMdiagram FSM Diagrams
        The finite state machine diagram for the encoder data collection looks like this: 
        @image html datagen_fsmdiagram.png
        
        The finite state machine diagram for the user interface looks like this:
        @image html ui_fsmdiagram.png
    @section sec_Lab04_TaskDiagram Task Diagram
        The task diagram for this lab looks like this:
        @image html Lab4TaskDiagram.png

    @section sec_Lab04_source Source Code
        The source code for the lab assignment can be found in this BitBucket 
        repository: 
        [<b>Lab 04 Repository</b>](https://bitbucket.org/bsahlin/me305_labs/src/master/Lab4/main_lab4.py)

    @section sec_Lab04_files Files
        @ref main_lab4.py \n
        @ref FSM_datagen.py \n
        @ref FSM_UI.py \n
        @ref encoder.py \n
        @ref shares.py \n

'''